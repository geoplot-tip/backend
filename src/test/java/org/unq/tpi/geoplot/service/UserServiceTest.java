package org.unq.tpi.geoplot.service;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.unq.tpi.geoplot.model.dto.AuthCredentials;

@SpringBootTest
public class UserServiceTest {
    @BeforeEach
    public void setUp() {
        userService.clean();
    }

    @Autowired
    private UserService userService;

    @Test
    public void should_return_an_empty_list_when_there_are_no_registered_users() {
        Assertions.assertEquals(0, userService.getAllUsers().size());
    }

    @Test
    public void should_return_a_list_of_users_when_there_are_registered_users() {
        userService.register(new AuthCredentials("prueba@gmail.com", "13456"));
        Assertions.assertEquals(1, userService.getAllUsers().size());
    }
}