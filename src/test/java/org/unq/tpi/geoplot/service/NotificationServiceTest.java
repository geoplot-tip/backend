package org.unq.tpi.geoplot.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import jakarta.validation.ConstraintViolationException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.unq.tpi.geoplot.model.Notification;
import org.unq.tpi.geoplot.model.User;
import org.unq.tpi.geoplot.model.dto.AuthCredentials;
import org.unq.tpi.geoplot.security.TokenUtils;
import org.unq.tpi.geoplot.utilities.exception.NotFoundException;
import org.unq.tpi.geoplot.utilities.exception.UserException;

import java.util.Objects;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;

@SpringBootTest
@AutoConfigureMockMvc
class NotificationServiceTest {

    @Autowired
    private MockMvc mockMvc;

    private ObjectMapper mapper = new ObjectMapper();

    @Autowired
    private NotificationService notificationService;

    @Autowired
    private UserService userService;

    private AuthCredentials user = new AuthCredentials("prueba@gmail.com", "13456");

    private String authorization;

    @BeforeEach
    public void setUp() throws Exception {
        notificationService.clean();
        userService.clean();
        userService.register(user);
        authorization = mockMvc.perform(
                post("/api/user/login")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(mapper.writeValueAsString(user))
                        .accept("application/json")
        ).andReturn().getResponse().getHeader("Authorization");
    }

    @Test
    public void should_create_a_notification_with_valid_data() {
        String email = Objects.requireNonNull(TokenUtils.getAuthentication(authorization.substring(7))).getPrincipal().toString();
        User user1 = userService.findByEmail(email);
        Notification notification = notificationService.register("danger", "Prueba", "Testing complete", user1);

        Assertions.assertEquals("danger", notification.getVariant());
        Assertions.assertEquals("Prueba", notification.getTitle());
        Assertions.assertEquals("Testing complete", notification.getContent());
        Assertions.assertEquals(user1.getId(), notification.getUser().getId());
    }


    @Test
    public void should_throw_an_exception_when_a_notification_is_created_without_a_variant() {
        ConstraintViolationException thrown = Assertions.assertThrows(ConstraintViolationException.class, () -> notificationService.register(null, null, null, null));
        boolean result = thrown.getConstraintViolations().stream().anyMatch(notification -> Objects.equals(notification.getMessage(), "The variant cannot be blank"));
        Assertions.assertTrue(result);
    }

    @Test
    public void should_throw_an_exception_when_a_notification_is_created_with_empty_a_variant() {
        ConstraintViolationException thrown = Assertions.assertThrows(ConstraintViolationException.class, () -> notificationService.register("", null, null, null));
        boolean result = thrown.getConstraintViolations().stream().anyMatch(notification -> Objects.equals(notification.getMessage(), "The variant cannot be blank"));
        Assertions.assertTrue(result);
    }

    @Test
    public void should_throw_an_exception_when_a_notification_is_created_without_a_title() {
        ConstraintViolationException thrown = Assertions.assertThrows(ConstraintViolationException.class, () -> notificationService.register("danger", null, null, null));
        boolean result = thrown.getConstraintViolations().stream().anyMatch(notification -> Objects.equals(notification.getMessage(), "The title cannot be blank"));
        Assertions.assertTrue(result);
    }

    @Test
    public void should_throw_an_exception_when_a_notification_is_created_with_empty_a_title() {
        ConstraintViolationException thrown = Assertions.assertThrows(ConstraintViolationException.class, () -> notificationService.register("danger", "", null, null));
        boolean result = thrown.getConstraintViolations().stream().anyMatch(notification -> Objects.equals(notification.getMessage(), "The title cannot be blank"));
        Assertions.assertTrue(result);
    }

    @Test
    public void should_throw_an_exception_when_a_notification_is_created_without_a_content() {
        ConstraintViolationException thrown = Assertions.assertThrows(ConstraintViolationException.class, () -> notificationService.register("danger", "Prueba", "", null));
        boolean result = thrown.getConstraintViolations().stream().anyMatch(notification -> Objects.equals(notification.getMessage(), "The content cannot be blank"));
        Assertions.assertTrue(result);
    }

    @Test
    public void should_throw_an_exception_when_a_notification_is_created_with_empty_a_content() {
        ConstraintViolationException thrown = Assertions.assertThrows(ConstraintViolationException.class, () -> notificationService.register("danger", "Prueba", "", null));
        boolean result = thrown.getConstraintViolations().stream().anyMatch(notification -> Objects.equals(notification.getMessage(), "The content cannot be blank"));
        Assertions.assertTrue(result);
    }

    @Test
    public void you_should_modify_the_deed_if_the_notification_exists_and_the_user_is_registered_to_it() {
        String email = Objects.requireNonNull(TokenUtils.getAuthentication(authorization.substring(7))).getPrincipal().toString();
        User user1 = userService.findByEmail(email);
        Notification notification = notificationService.register("danger", "Prueba", "Testing complete", user1);
        boolean statusOld = notification.isRead();
        notificationService.markAsRead(notification.getId(), authorization);

        assertTrue(notificationService.getNotifications(authorization).get(0).isRead() != statusOld);
    }

    @Test
    public void should_throw_an_exception_if_the_notification_is_not_registered() {
        NotFoundException thrown = Assertions.assertThrows(NotFoundException.class, () -> notificationService.markAsRead(30, authorization));
        Assertions.assertEquals("The notification is not registered", thrown.getMessage());
    }

    @Test
    public void you_should_throw_an_exception_if_the_notification_is_not_registered_to_a_valid_user() {
        String email = Objects.requireNonNull(TokenUtils.getAuthentication(authorization.substring(7))).getPrincipal().toString();
        User user1 = userService.findByEmail(email);
        Notification notification = notificationService.register("danger", "Prueba", "Testing complete", user1);

        UserException thrown = Assertions.assertThrows(UserException.class, () -> notificationService.markAsRead(notification.getId(), "Bearer eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJhZG1pbnNzc3Nzc0BnbWFpbC5jb20iLCJleHAiOjE3ODU4ODQ0Mzl9.8LAvBkZBqbGH3NE1w2ACxSN0jv_WoYihneDfITq7DJM"));
        Assertions.assertEquals("The user is not related to this notification", thrown.getMessage());
    }

}