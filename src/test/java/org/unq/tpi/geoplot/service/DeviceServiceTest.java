package org.unq.tpi.geoplot.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.test.web.servlet.MockMvc;
import org.unq.tpi.geoplot.model.Device;
import org.unq.tpi.geoplot.model.User;
import org.unq.tpi.geoplot.model.dto.OperateUserDevice;
import org.unq.tpi.geoplot.model.dto.AuthCredentials;
import org.unq.tpi.geoplot.model.dto.DeviceRegisterRequest;
import org.unq.tpi.geoplot.utilities.exception.DeviceException;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;

@SpringBootTest
@AutoConfigureMockMvc
class DeviceServiceTest {

    @Autowired
    private MockMvc mockMvc;

    private ObjectMapper mapper = new ObjectMapper();

    @Autowired
    private DeviceService deviceService;

    @Autowired
    private UserService userService;

    private AuthCredentials user = new AuthCredentials("prueba@gmail.com", "13456");

    private String authorization;

    @BeforeEach
    public void setUp() throws Exception {
        deviceService.clean();
        userService.clean();
        userService.register(user);
        authorization = mockMvc.perform(
                post("/api/user/login")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(mapper.writeValueAsString(user))
                        .accept("application/json")
        ).andReturn().getResponse().getHeader("Authorization");
    }

    @Test
    public void should_throw_an_exception_when_you_want_to_add_an_already_registered_user_to_a_device() {
        Device device = deviceService.register(authorization, new DeviceRegisterRequest("Arduino 1", "192.158.4.36"));
        User user = userService.findByEmail("prueba@gmail.com");
        DeviceException thrown = Assertions.assertThrows(DeviceException.class, () -> deviceService.addUser( new OperateUserDevice(user.getId(), device.getId())));
        Assertions.assertEquals("The user is already included in this device", thrown.getMessage());
    }

    @Test
    public void should_throw_an_exception_when_a_device_is_registered_without_a_registered_user() {
        UsernameNotFoundException thrown = Assertions.assertThrows(UsernameNotFoundException.class, () -> deviceService.register("Bearer eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJhZG1pbnNzQGdtYWlsLmNvbSIsImV4cCI6MTc4NTg4MzA4NX0.kUIC2aVXZ1S-lQO6czo1aqaXUyXltVZx3bPJW3RTjrE", new DeviceRegisterRequest("Arduino 1", "192.158.4.36")));
        Assertions.assertEquals("User not found", thrown.getMessage());
    }

    @Test
    public void should_throw_an_exception_when_the_devices_of_an_unregistered_user_are_searched() throws Exception {
        UsernameNotFoundException thrown = Assertions.assertThrows(UsernameNotFoundException.class, () ->
                deviceService.getDevices("Bearer eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJhZG1pbnNzc3Nzc0BnbWFpbC5jb20iLCJleHAiOjE3ODU4ODQ0Mzl9.8LAvBkZBqbGH3NE1w2ACxSN0jv_WoYihneDfITq7DJM"));
        Assertions.assertEquals("User not found", thrown.getMessage());
    }
}