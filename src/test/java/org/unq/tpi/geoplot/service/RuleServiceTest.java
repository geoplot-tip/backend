package org.unq.tpi.geoplot.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.unq.tpi.geoplot.model.Rule;
import org.unq.tpi.geoplot.model.dto.AuthCredentials;
import org.unq.tpi.geoplot.model.dto.ruleForms.LimitRuleForm;
import org.unq.tpi.geoplot.utilities.exception.NotFoundException;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;

@SpringBootTest
@AutoConfigureMockMvc
class RuleServiceTest {

    @Autowired
    private MockMvc mockMvc;

    private ObjectMapper mapper = new ObjectMapper();

    @Autowired
    private RuleService ruleService;

    @Autowired
    private UserService userService;

    private AuthCredentials user = new AuthCredentials("prueba@gmail.com", "13456");

    private String authorization;

    @BeforeEach
    public void setUp() throws Exception {
        ruleService.clean();
        userService.clean();
        userService.register(user);
        authorization = mockMvc.perform(
                post("/api/user/login")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(mapper.writeValueAsString(user))
                        .accept("application/json")
        ).andReturn().getResponse().getHeader("Authorization");
    }

    @Test
    public void should_find_a_rule_if_you_are_registered() {
        Rule rule = ruleService.register(new Rule("Prueba"), new LimitRuleForm());
        Rule ruleFind = ruleService.findByName("Prueba");

        Assertions.assertEquals(rule.getId(), ruleFind.getId());
        Assertions.assertEquals(rule.getName(), ruleFind.getName());
    }

    @Test
    public void It_should_throw_an_exception_when_the_rule_not_found_with_the_name() {
        NotFoundException thrown = Assertions.assertThrows(NotFoundException.class, () -> ruleService.findByName("Test"));
        Assertions.assertEquals("The rule with name is not registered", thrown.getMessage());
    }
}