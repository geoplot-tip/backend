package org.unq.tpi.geoplot.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.unq.tpi.geoplot.model.dto.AuthCredentials;
import org.unq.tpi.geoplot.model.dto.DeviceRegisterRequest;
import org.unq.tpi.geoplot.model.dto.SensorRegisterRequest;
import org.unq.tpi.geoplot.model.dto.UpdateSensorData;
import org.unq.tpi.geoplot.service.DeviceService;
import org.unq.tpi.geoplot.service.SensorService;
import org.unq.tpi.geoplot.service.UserService;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
public class SensorControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private SensorService sensorService;
    @Autowired
    private DeviceService deviceService;
    @Autowired
    private UserService userService;
    private ObjectMapper mapper = new ObjectMapper();
    private AuthCredentials user = new AuthCredentials("prueba@gmail.com", "13456");

    private String authorization;

    public SensorControllerTest() {
    }

    @BeforeEach
    public void setUp() throws Exception {
        deviceService.clean();
        sensorService.clean();
        userService.clean();
        userService.register(user);
        authorization = mockMvc.perform(
                post("/api/user/login")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(mapper.writeValueAsString(user))
                        .accept("application/json")
        ).andReturn().getResponse().getHeader("Authorization");

    }

    @Test
    void should_throw_a_OK_status_when_a_valid_sensor_is_registered() throws Exception {
        int deviceId = deviceService.register(authorization, new DeviceRegisterRequest("Arduino 1", "192.168.0.20")).getId();
        mockMvc.perform(post("/api/sensor/register")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(mapper.writeValueAsString(new SensorRegisterRequest(deviceId, "Temperatura", "DH11", "192.168.0.5","C","Temperatura 1")))
                        .header("Authorization", authorization)
                        .accept("application/json"))
                .andExpect(status().isOk());
    }

    @Test
    void should_throw_a_NotFound_error_when_registering_a_sensor_with_unregistered_device() throws Exception {
        mockMvc.perform(post("/api/sensor/register")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(mapper.writeValueAsString(new SensorRegisterRequest(8, "Temperatura", "DH11", "192.168.0.5","C","Temperatura 1")))
                        .header("Authorization", authorization)
                        .accept("application/json"))
                .andExpect(status().isNotFound());
    }

    @Test
    void should_throw_a_BadRequest_error_when_registering_a_sensor_with_null_model() throws Exception {
        int deviceId = deviceService.register(authorization, new DeviceRegisterRequest("Arduino 1", "192.168.0.20")).getId();
        mockMvc.perform(post("/api/sensor/register")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(mapper.writeValueAsString(new SensorRegisterRequest(deviceId, null, null, null,null,null)))
                        .header("Authorization", authorization)
                        .accept("application/json"))
                .andExpect(status().isBadRequest());
    }

    @Test
    void should_throw_a_BadRequest_error_when_registering_a_sensor_with_empty_model() throws Exception {
        int deviceId = deviceService.register(authorization, new DeviceRegisterRequest("Arduino 1", "192.168.0.20")).getId();
        mockMvc.perform(post("/api/sensor/register")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(mapper.writeValueAsString(new SensorRegisterRequest(deviceId, "", null, null,null,null)))
                        .header("Authorization", authorization)
                        .accept("application/json"))
                .andExpect(status().isBadRequest());
    }

    @Test
    void should_throw_a_BadRequest_error_when_registering_a_sensor_with_null_type() throws Exception {
        int deviceId = deviceService.register(authorization, new DeviceRegisterRequest("Arduino 1", "192.168.0.20")).getId();
        mockMvc.perform(post("/api/sensor/register")
                        .contentType(MediaType.APPLICATION_JSON)
                        .header("Authorization", authorization)
                        .content(mapper.writeValueAsString(new SensorRegisterRequest(deviceId, "Temperatura", null, null,null,null)))
                        .accept("application/json"))
                .andExpect(status().isBadRequest());
    }

    @Test
    void should_throw_a_BadRequest_error_when_registering_a_sensor_with_empty_type() throws Exception {
        int deviceId = deviceService.register(authorization, new DeviceRegisterRequest("Arduino 1", "192.168.0.20")).getId();
        mockMvc.perform(post("/api/sensor/register")
                        .contentType(MediaType.APPLICATION_JSON)
                        .header("Authorization", authorization)
                        .content(mapper.writeValueAsString(new SensorRegisterRequest(deviceId, "Temperatura", "", null,null,null)))
                        .accept("application/json"))
                .andExpect(status().isBadRequest());
    }

    @Test
    void it_should_throw_a_isForbidden_error_when_a_sensor_is_registered_without_being_logged_in() throws Exception {
        int deviceId = deviceService.register(authorization, new DeviceRegisterRequest("Arduino 1", "192.168.0.20")).getId();
        mockMvc.perform(post("/api/sensor/register")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(mapper.writeValueAsString(new SensorRegisterRequest(deviceId, "Temperatura", "", null,null,null)))
                        .accept("application/json"))
                .andExpect(status().isForbidden());
    }

    @Test
    void should_throw_a_OK_status_when_updating_a_valid_sensor() throws Exception {
        int deviceId = deviceService.register(authorization, new DeviceRegisterRequest("Arduino 1", "192.168.0.20")).getId();
        int sensorId = sensorService.register(new SensorRegisterRequest(deviceId, "Temperatura", "DH11", "192.168.0.5","C","Temperatura 1")).getId();

        mockMvc.perform(post("/api/sensor/updateData")
                        .contentType(MediaType.APPLICATION_JSON)
                        .header("Authorization", authorization)
                        .content(mapper.writeValueAsString(new UpdateSensorData(sensorId, "20.0")))
                        .accept("application/json"))
                .andExpect(status().isOk());
    }


    @Test
    void should_throw_a_NotFound_error_when_updating_an_unregistered_sensor() throws Exception {
        int deviceId = deviceService.register(authorization, new DeviceRegisterRequest("Arduino 1", "192.168.0.20")).getId();
        sensorService.register(new SensorRegisterRequest(deviceId, "Temperatura", "DH11", "192.168.0.5","C","Temperatura 1"));

        mockMvc.perform(post("/api/sensor/updateData")
                        .contentType(MediaType.APPLICATION_JSON)
                        .header("Authorization", authorization)
                        .content(mapper.writeValueAsString(new UpdateSensorData(8, "20.0")))
                        .accept("application/json"))
                .andExpect(status().isNotFound());
    }

    @Test
    void should_throw_a_NotFound_error_when_updating_a_sensor_with_null_value() throws Exception {
        int deviceId = deviceService.register(authorization, new DeviceRegisterRequest("Arduino 1", "192.168.0.20")).getId();
        sensorService.register(new SensorRegisterRequest(deviceId, "Temperatura", "DH11", "192.168.0.5","C","Temperatura 1"));

        mockMvc.perform(post("/api/sensor/updateData")
                        .contentType(MediaType.APPLICATION_JSON)
                        .header("Authorization", authorization)
                        .content(mapper.writeValueAsString(new UpdateSensorData(4, null)))
                        .accept("application/json"))
                .andExpect(status().isBadRequest());
    }

    @Test
    void should_throw_a_BadRequest_error_when_updating_a_sensor_with_empty_value() throws Exception {
        int deviceId = deviceService.register(authorization, new DeviceRegisterRequest("Arduino 1", "192.168.0.20")).getId();
        sensorService.register(new SensorRegisterRequest(deviceId, "Temperatura", "DH11", "192.168.0.5","C","Temperatura 1"));

        mockMvc.perform(post("/api/sensor/updateData")
                        .contentType(MediaType.APPLICATION_JSON)
                        .header("Authorization", authorization)
                        .content(mapper.writeValueAsString(new UpdateSensorData(4, "")))
                        .accept("application/json"))
                .andExpect(status().isBadRequest());
    }

    @Test
    void it_should_throw_a_isForbidden_error_when_a_sensor_is_updating_without_being_logged_in() throws Exception {
        int deviceId = deviceService.register(authorization, new DeviceRegisterRequest("Arduino 1", "192.168.0.20")).getId();
        sensorService.register(new SensorRegisterRequest(deviceId, "Temperatura", "DH11", "192.168.0.5","C","Temperatura 1"));

        mockMvc.perform(post("/api/sensor/updateData")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(mapper.writeValueAsString(new UpdateSensorData(4, "")))
                        .accept("application/json"))
                .andExpect(status().isForbidden());
    }

}
