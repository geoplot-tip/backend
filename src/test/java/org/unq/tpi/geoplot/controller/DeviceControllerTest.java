package org.unq.tpi.geoplot.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.web.servlet.MockMvc;
import org.unq.tpi.geoplot.model.Device;
import org.unq.tpi.geoplot.model.Rule;
import org.unq.tpi.geoplot.model.RuleCondition;
import org.unq.tpi.geoplot.model.User;
import org.unq.tpi.geoplot.model.dto.*;
import org.unq.tpi.geoplot.model.dto.ruleForms.LimitRuleForm;
import org.unq.tpi.geoplot.service.DeviceService;
import org.unq.tpi.geoplot.service.RuleService;
import org.unq.tpi.geoplot.service.UserService;

import java.util.List;
import java.util.Map;
import java.util.Objects;

import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
public class DeviceControllerTest {

    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private DeviceService deviceService;
    @Autowired
    private UserService userService;
    @Autowired
    private RuleService ruleService;
    private final ObjectMapper mapper = new ObjectMapper();
    private final AuthCredentials user = new AuthCredentials("prueba@gmail.com", "13456");

    private String authorization;

    @BeforeEach
    public void setUp() throws Exception {
        deviceService.clean();
        ruleService.clean();
        userService.clean();
        userService.register(user);
        authorization = mockMvc.perform(
                post("/api/user/login")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(mapper.writeValueAsString(user))
                        .accept("application/json")
        ).andReturn().getResponse().getHeader("Authorization");
    }

    @Test
    void should_throw_a_OK_status_when_a_valid_device_is_registered() throws Exception {
        MockHttpServletResponse responseEntity = mockMvc.perform(post("/api/device/register")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(mapper.writeValueAsString(new DeviceRegisterRequest("Arduino 1", "192.168.0.20")))
                        .header("Authorization", authorization)
                        .accept("application/json"))
                .andExpect(status().isOk()).andReturn().getResponse();
        Device device = mapper.readValue(responseEntity.getContentAsString(), Device.class);

        assertEquals("Arduino 1", device.getName());
        assertEquals("192.168.0.20", device.getServerUrl());
    }

    @Test
    void should_throw_bad_request_error_when_registered_a_device_with_null_name() throws Exception {
        MockHttpServletResponse responseEntity = mockMvc.perform(post("/api/device/register")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(mapper.writeValueAsString(new DeviceRegisterRequest(null, "192.168.0.20")))
                        .header("Authorization", authorization)
                        .accept("application/json"))
                .andExpect(status().isBadRequest()).andReturn().getResponse();
        Map message = mapper.readValue(responseEntity.getContentAsString(), Map.class);

        assertEquals("The device name cannot be blank", message.get("name"));
    }

    @Test
    void should_throw_bad_request_error_when_registered_a_device_with_empty_name() throws Exception {
        MockHttpServletResponse responseEntity = mockMvc.perform(post("/api/device/register")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(mapper.writeValueAsString(new DeviceRegisterRequest("", "192.168.0.20")))
                        .header("Authorization", authorization)
                        .accept("application/json"))
                .andExpect(status().isBadRequest()).andReturn().getResponse();
        Map message = mapper.readValue(responseEntity.getContentAsString(), Map.class);

        assertEquals("The device name cannot be blank", message.get("name"));
    }

    @Test
    void should_throw_bad_request_error_when_registered_a_device_with_null_server() throws Exception {
        MockHttpServletResponse responseEntity = mockMvc.perform(post("/api/device/register")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(mapper.writeValueAsString(new DeviceRegisterRequest("Arduino 1", null)))
                        .header("Authorization", authorization)
                        .accept("application/json"))
                .andExpect(status().isBadRequest()).andReturn().getResponse();
        Map message = mapper.readValue(responseEntity.getContentAsString(), Map.class);

        assertEquals("The server url cannot be blank", message.get("serverUrl"));
    }

    @Test
    void should_throw_bad_request_error_when_registered_a_device_with_empty_server() throws Exception {
        MockHttpServletResponse responseEntity = mockMvc.perform(post("/api/device/register")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(mapper.writeValueAsString(new DeviceRegisterRequest("Arduino 1", "")))
                        .header("Authorization", authorization)
                        .accept("application/json"))
                .andExpect(status().isBadRequest()).andReturn().getResponse();
        Map message = mapper.readValue(responseEntity.getContentAsString(), Map.class);

        assertEquals("The server url cannot be blank", message.get("serverUrl"));
    }

    @Test
    void should_throw_bad_request_error_when_registered_a_device_already_registered() throws Exception {
        Device device = deviceService.register(authorization, new DeviceRegisterRequest("Arduino 1", "192.168.0.20"));
        MockHttpServletResponse responseEntity = mockMvc.perform(post("/api/device/register")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(mapper.writeValueAsString(new DeviceRegisterRequest("Arduino 2", "192.168.0.20")))
                        .header("Authorization", authorization)
                        .accept("application/json"))
                .andExpect(status().isBadRequest()).andReturn().getResponse();
        Map message = mapper.readValue(responseEntity.getContentAsString(), Map.class);

        assertEquals("The device has already been registered", message.get("message"));
    }

    @Test
    void should_throw_bad_request_error_when_registered_a_device_without_authentication() throws Exception {
        mockMvc.perform(post("/api/device/register")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(mapper.writeValueAsString(new DeviceRegisterRequest("Arduino 1", "")))
                        .accept("application/json"))
                .andExpect(status().isForbidden());
    }

    @Test
    void should_throw_a_OK_status_when_add_a_valid_user() throws Exception {
        Device device = deviceService.register(authorization, new DeviceRegisterRequest("Arduino 1", "192.168.0.20"));
        User newUser = userService.register(new AuthCredentials("nuevo@gmail.com", "132"));
        MockHttpServletResponse responseEntity = mockMvc.perform(post("/api/device/addUser")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(mapper.writeValueAsString(new OperateUserDevice(newUser.getId(), device.getId())))
                        .header("Authorization", authorization)
                        .accept("application/json"))
                .andExpect(status().isOk()).andReturn().getResponse();
        Device deviceResult = mapper.readValue(responseEntity.getContentAsString(), Device.class);

        assertTrue(deviceResult.getUserList().stream().anyMatch(user1 -> Objects.equals(user1.getEmail(), newUser.getEmail())));
    }

    @Test
    void should_throw_a_BadRequest_status_when_adding_a_user_with_invalid_id() throws Exception {
        Device device = deviceService.register(authorization, new DeviceRegisterRequest("Arduino 1", "192.168.0.20"));
        MockHttpServletResponse responseEntity = mockMvc.perform(post("/api/device/addUser")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(mapper.writeValueAsString(new OperateUserDevice(0, device.getId())))
                        .header("Authorization", authorization)
                        .accept("application/json"))
                .andExpect(status().isBadRequest()).andReturn().getResponse();
        Map message = mapper.readValue(responseEntity.getContentAsString(), Map.class);

        assertEquals("User id's start from 1 onwards", message.get("userId"));
    }

    @Test
    void should_throw_a_NotFound_status_when_adding_an_unregistered_user() throws Exception {
        Device device = deviceService.register(authorization, new DeviceRegisterRequest("Arduino 1", "192.168.0.20"));
        MockHttpServletResponse responseEntity = mockMvc.perform(post("/api/device/addUser")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(mapper.writeValueAsString(new OperateUserDevice(22, device.getId())))
                        .header("Authorization", authorization)
                        .accept("application/json"))
                .andExpect(status().isNotFound()).andReturn().getResponse();
        Map message = mapper.readValue(responseEntity.getContentAsString(), Map.class);

        assertEquals("User not found", message.get("message"));
    }

    @Test
    void should_throw_a_BadRequest_status_when_adding_an_already_registered_user() throws Exception {
        Device device = deviceService.register(authorization, new DeviceRegisterRequest("Arduino 1", "192.168.0.20"));
        User newUser = userService.register(new AuthCredentials("nuevo@gmail.com", "132"));
        deviceService.addUser(new OperateUserDevice(newUser.getId(), device.getId()));
        MockHttpServletResponse responseEntity = mockMvc.perform(post("/api/device/addUser")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(mapper.writeValueAsString(new OperateUserDevice(newUser.getId(), device.getId())))
                        .header("Authorization", authorization)
                        .accept("application/json"))
                .andExpect(status().isBadRequest()).andReturn().getResponse();
        Map message = mapper.readValue(responseEntity.getContentAsString(), Map.class);

        assertEquals("The user is already included in this device", message.get("message"));
    }

    @Test
    void should_throw_a_OK_status_when_searching_for_all_devices_of_a_valid_user() throws Exception {
        Device device = deviceService.register(authorization, new DeviceRegisterRequest("Arduino 1", "192.168.0.20"));
        MockHttpServletResponse responseEntity = mockMvc.perform(get("/api/device/devices")
                        .contentType(MediaType.APPLICATION_JSON)
                        .header("Authorization", authorization)
                        .accept("application/json"))
                .andExpect(status().isOk()).andReturn().getResponse();
        List deviceList = mapper.readValue(responseEntity.getContentAsString(), List.class);
        assertEquals(1, deviceList.size());
    }

    @Test
    void should_throw_a_Forbidden_status_when_searching_for_all_devices_without_a_token() throws Exception {
        deviceService.register(authorization, new DeviceRegisterRequest("Arduino 1", "192.168.0.20"));
        mockMvc.perform(get("/api/device/devices")
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept("application/json"))
                .andExpect(status().isForbidden());
    }

    @Test
    void should_throw_a_OK_status_when_searching_for_all_sensors() throws Exception {
        Device device = deviceService.register(authorization, new DeviceRegisterRequest("Arduino 1", "192.168.0.20"));
        MockHttpServletResponse responseEntity = mockMvc.perform(get("/api/device/getSensors")
                        .contentType(MediaType.APPLICATION_JSON)
                        .param("device_id", String.valueOf(device.getId()))
                        .header("Authorization", authorization)
                        .accept("application/json"))
                .andExpect(status().isOk()).andReturn().getResponse();
        List sensorList = mapper.readValue(responseEntity.getContentAsString(), List.class);
        assertTrue(sensorList.isEmpty());
    }

    @Test
    void should_throw_a_Forbidden_status_when_searching_for_all_sensors_without_a_token() throws Exception {
        Device device = deviceService.register(authorization, new DeviceRegisterRequest("Arduino 1", "192.168.0.20"));
        mockMvc.perform(get("/api/device/getSensors")
                        .contentType(MediaType.APPLICATION_JSON)
                        .param("device_id", String.valueOf(device.getId()))
                        .accept("application/json"))
                .andExpect(status().isForbidden());
    }

    @Test
    void should_throw_a_OK_status_when_latitude_and_longitude_are_updated_correctly() throws Exception {
        Device device = deviceService.register(authorization, new DeviceRegisterRequest("Arduino 1", "192.168.0.20"));
        MockHttpServletResponse responseEntity = mockMvc.perform(post("/api/device/setLocation")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(mapper.writeValueAsString(new DeviceSetLocationRequest(device.getId(), 200.00, 20.0)))
                        .header("Authorization", authorization)
                        .accept("application/json"))
                .andExpect(status().isOk()).andReturn().getResponse();
        Device deviceResult = mapper.readValue(responseEntity.getContentAsString(), Device.class);

        assertEquals(200.00, deviceResult.getLatitude());
        assertEquals(20.0, deviceResult.getLongitude());
    }

    @Test
    void should_throw_a_OK_status_when_updating_latitude_and_longitude_an_unregistered_device() throws Exception {
        deviceService.register(authorization, new DeviceRegisterRequest("Arduino 1", "192.168.0.20"));
        mockMvc.perform(post("/api/device/setLocation")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(mapper.writeValueAsString(new DeviceSetLocationRequest(-20, 200.00, 20.0)))
                        .accept("application/json"))
                .andExpect(status().isForbidden());
    }

    @Test
    void should_throw_a_OK_status_when_latitude_and_longitude_without_a_token() throws Exception {
        Device device = deviceService.register(authorization, new DeviceRegisterRequest("Arduino 1", "192.168.0.20"));
        mockMvc.perform(post("/api/device/setLocation")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(mapper.writeValueAsString(new DeviceSetLocationRequest(device.getId(), 200.00, 20.0)))
                        .accept("application/json"))
                .andExpect(status().isForbidden());
    }

    @Test
    void should_throw_a_OK_status_when_a_rule_is_added_to_the_device() throws Exception {
        Device device = deviceService.register(authorization, new DeviceRegisterRequest("Arduino 1", "192.168.0.20"));
        Rule rule = ruleService.register(new Rule("Active"), new LimitRuleForm());
        ruleService.register(new Rule("Inactive"), new LimitRuleForm());
        MockHttpServletResponse responseEntity = mockMvc.perform(post("/api/device/addRule")
                        .contentType(MediaType.APPLICATION_JSON)
                        .param("device_id", String.valueOf(device.getId()))
                        .param("rule_id", String.valueOf(rule.getId()))
                        .header("Authorization", authorization)
                        .accept("application/json"))
                .andExpect(status().isOk()).andReturn().getResponse();
        List ruleList = mapper.readValue(responseEntity.getContentAsString(), List.class);

        assertTrue(!ruleList.isEmpty());
    }

    @Test
    void should_throw_a_NotFound_status_when_a_non_existent_rule_is_added() throws Exception {
        Device device = deviceService.register(authorization, new DeviceRegisterRequest("Arduino 1", "192.168.0.20"));
        MockHttpServletResponse responseEntity = mockMvc.perform(post("/api/device/addRule")
                        .contentType(MediaType.APPLICATION_JSON)
                        .param("device_id", String.valueOf(device.getId()))
                        .param("rule_id", "-5")
                        .header("Authorization", authorization)
                        .accept("application/json"))
                .andExpect(status().isNotFound()).andReturn().getResponse();
        Map message = mapper.readValue(responseEntity.getContentAsString(), Map.class);

        assertEquals("The rule is not registered", message.get("message"));
    }

    @Test
    void should_throw_a_NotFound_status_when_a_rule_is_added_with_non_existent_device() throws Exception {
        MockHttpServletResponse responseEntity = mockMvc.perform(post("/api/device/addRule")
                        .contentType(MediaType.APPLICATION_JSON)
                        .param("device_id", "-5")
                        .param("rule_id", "-5")
                        .header("Authorization", authorization)
                        .accept("application/json"))
                .andExpect(status().isNotFound()).andReturn().getResponse();
        Map message = mapper.readValue(responseEntity.getContentAsString(), Map.class);

        assertEquals("The device is not registered", message.get("message"));
    }

    @Test
    void should_throw_a_BadRequest_status_when_an_already_registered_rule_is_added() throws Exception {
        Device device = deviceService.register(authorization, new DeviceRegisterRequest("Arduino 1", "192.168.0.20"));
        Rule rule = ruleService.register(new Rule("Active"), new LimitRuleForm());
        ruleService.register(new Rule("Inactive"), new LimitRuleForm());
        RuleConditionDTO ruleConditionDTO = new RuleConditionDTO(0, 1, 1);
        deviceService.addRule(device.getId(), rule.getId(), ruleConditionDTO);
        MockHttpServletResponse responseEntity = mockMvc.perform(post("/api/device/addRule")
                        .contentType(MediaType.APPLICATION_JSON)
                        .param("device_id", String.valueOf(device.getId()))
                        .param("rule_id", String.valueOf(rule.getId()))
                        .header("Authorization", authorization)
                        .accept("application/json"))
                .andExpect(status().isBadRequest()).andReturn().getResponse();
        Map message = mapper.readValue(responseEntity.getContentAsString(), Map.class);

        assertEquals("The rule is already included", message.get("message"));
    }

    @Test
    void should_throw_a_OK_status_when_a_registered_rule_is_deleted() throws Exception {
        Device device = deviceService.register(authorization, new DeviceRegisterRequest("Arduino 1", "192.168.0.20"));
        Rule rule = ruleService.register(new Rule("Active"), new LimitRuleForm());
        ruleService.register(new Rule("Inactive"), new LimitRuleForm());
        RuleConditionDTO ruleConditionDTO = new RuleConditionDTO(0, 1, 1);
        deviceService.addRule(device.getId(), rule.getId(), ruleConditionDTO);
        MockHttpServletResponse responseEntity = mockMvc.perform(post("/api/device/removeRule")
                        .contentType(MediaType.APPLICATION_JSON)
                        .param("device_id", String.valueOf(device.getId()))
                        .param("rule_id", String.valueOf(rule.getId()))
                        .header("Authorization", authorization)
                        .accept("application/json"))
                .andExpect(status().isOk()).andReturn().getResponse();
        List ruleList = mapper.readValue(responseEntity.getContentAsString(), List.class);

        assertTrue(ruleList.isEmpty());
    }

    @Test
    void should_throw_a_NotFound_status_when_an_unregistered_rule_is_deleted() throws Exception {
        Device device = deviceService.register(authorization, new DeviceRegisterRequest("Arduino 1", "192.168.0.20"));
        MockHttpServletResponse responseEntity = mockMvc.perform(post("/api/device/removeRule")
                        .contentType(MediaType.APPLICATION_JSON)
                        .param("device_id", String.valueOf(device.getId()))
                        .param("rule_id", "-1")
                        .header("Authorization", authorization)
                        .accept("application/json"))
                .andExpect(status().isNotFound()).andReturn().getResponse();
        Map message = mapper.readValue(responseEntity.getContentAsString(), Map.class);

        assertEquals("The rule is not registered", message.get("message"));
    }

    @Test
    void should_throw_a_NotFound_status_when_an_unregistered_device_is_deleted() throws Exception {
        deviceService.register(authorization, new DeviceRegisterRequest("Arduino 1", "192.168.0.20"));
        MockHttpServletResponse responseEntity = mockMvc.perform(post("/api/device/removeRule")
                        .contentType(MediaType.APPLICATION_JSON)
                        .param("device_id", "-50")
                        .param("rule_id", "-1")
                        .header("Authorization", authorization)
                        .accept("application/json"))
                .andExpect(status().isNotFound()).andReturn().getResponse();
        Map message = mapper.readValue(responseEntity.getContentAsString(), Map.class);

        assertEquals("The device is not registered", message.get("message"));
    }

    @Test
    void should_throw_a_BadRequest_status_when_a_rule_that_is_not_associated_with_the_device_is_deleted() throws Exception {
        Device device = deviceService.register(authorization, new DeviceRegisterRequest("Arduino 1", "192.168.0.20"));
        Rule rule = ruleService.register(new Rule("Active"), new LimitRuleForm());
        ruleService.register(new Rule("Inactive"), new LimitRuleForm());
        MockHttpServletResponse responseEntity = mockMvc.perform(post("/api/device/removeRule")
                        .contentType(MediaType.APPLICATION_JSON)
                        .param("device_id", String.valueOf(device.getId()))
                        .param("rule_id", String.valueOf(rule.getId()))
                        .header("Authorization", authorization)
                        .accept("application/json"))
                .andExpect(status().isBadRequest()).andReturn().getResponse();
        Map message = mapper.readValue(responseEntity.getContentAsString(), Map.class);

        assertEquals("The rule is not registered on the device", message.get("message"));
    }

    @Test
    void should_return_a_list_of_empty_rules_when_no_rules_are_registered() throws Exception {
        Device device = deviceService.register(authorization, new DeviceRegisterRequest("Arduino 1", "192.168.0.20"));
        MockHttpServletResponse responseEntity = mockMvc.perform(get("/api/device/activeRules")
                        .contentType(MediaType.APPLICATION_JSON)
                        .param("device_id", String.valueOf(device.getId()))
                        .header("Authorization", authorization)
                        .accept("application/json"))
                .andExpect(status().isOk()).andReturn().getResponse();
        List rules = mapper.readValue(responseEntity.getContentAsString(), List.class);

        assertEquals(0, rules.size());
    }

    @Test
    void should_return_a_full_list_of_rules_when_rules_are_registered() throws Exception {
        Device device = deviceService.register(authorization, new DeviceRegisterRequest("Arduino 1", "192.168.0.20"));
        Rule rule = ruleService.register(new Rule("Active"), new LimitRuleForm());
        RuleConditionDTO ruleConditionDTO = new RuleConditionDTO(0, 1, 1);
        deviceService.addRule(device.getId(), rule.getId(), ruleConditionDTO);
        MockHttpServletResponse responseEntity = mockMvc.perform(get("/api/device/activeRules")
                        .contentType(MediaType.APPLICATION_JSON)
                        .param("device_id", String.valueOf(device.getId()))
                        .header("Authorization", authorization)
                        .accept("application/json"))
                .andExpect(status().isOk()).andReturn().getResponse();
        List rules = mapper.readValue(responseEntity.getContentAsString(), List.class);

        assertEquals(1, rules.size());
    }

    @Test
    void should_return_an_empty_list_when_no_users_are_available() throws Exception {
        Device device = deviceService.register(authorization, new DeviceRegisterRequest("Arduino 1", "192.168.0.20"));
        MockHttpServletResponse responseEntity = mockMvc.perform(get("/api/device/availableUsers")
                        .contentType(MediaType.APPLICATION_JSON)
                        .param("device_id", String.valueOf(device.getId()))
                        .header("Authorization", authorization)
                        .accept("application/json"))
                .andExpect(status().isOk()).andReturn().getResponse();
        List users = mapper.readValue(responseEntity.getContentAsString(), List.class);

        assertEquals(0, users.size());
    }

    @Test
    void should_return_a_full_list_when_users_are_available() throws Exception {
        Device device = deviceService.register(authorization, new DeviceRegisterRequest("Arduino 1", "192.168.0.20"));
        userService.register(new AuthCredentials("nuevo@gmail.com", "132"));

        MockHttpServletResponse responseEntity = mockMvc.perform(get("/api/device/availableUsers")
                        .contentType(MediaType.APPLICATION_JSON)
                        .param("device_id", String.valueOf(device.getId()))
                        .header("Authorization", authorization)
                        .accept("application/json"))
                .andExpect(status().isOk()).andReturn().getResponse();
        List users = mapper.readValue(responseEntity.getContentAsString(), List.class);

        assertEquals(1, users.size());
    }

    @Test
    void should_throw_a_OK_status_when_remove_a_valid_user() throws Exception {
        Device device = deviceService.register(authorization, new DeviceRegisterRequest("Arduino 1", "192.168.0.20"));
        User newUser = userService.register(new AuthCredentials("nuevo@gmail.com", "132"));
        deviceService.addUser(new OperateUserDevice(newUser.getId(), device.getId()));

        MockHttpServletResponse responseEntity = mockMvc.perform(post("/api/device/removeUser")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(mapper.writeValueAsString(new OperateUserDevice(newUser.getId(), device.getId())))
                        .header("Authorization", authorization)
                        .accept("application/json"))
                .andExpect(status().isOk()).andReturn().getResponse();
        Device deviceResult = mapper.readValue(responseEntity.getContentAsString(), Device.class);

        assertFalse(deviceResult.getUserList().stream().anyMatch(user1 -> Objects.equals(user1.getEmail(), newUser.getEmail())));
    }
}