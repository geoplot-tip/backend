package org.unq.tpi.geoplot.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.web.servlet.MockMvc;
import org.unq.tpi.geoplot.model.Rule;
import org.unq.tpi.geoplot.model.dto.AuthCredentials;
import org.unq.tpi.geoplot.model.dto.ruleForms.LimitRuleForm;
import org.unq.tpi.geoplot.service.RuleService;
import org.unq.tpi.geoplot.service.UserService;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
public class RuleControllerTest {

    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private UserService userService;
    @Autowired
    private RuleService ruleService;
    private final ObjectMapper mapper = new ObjectMapper();
    private final AuthCredentials user = new AuthCredentials("prueba@gmail.com", "13456");

    private String authorization;

    @BeforeEach
    public void setUp() throws Exception {
        ruleService.clean();
        userService.clean();
        userService.register(user);
        authorization = mockMvc.perform(
                post("/api/user/login")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(mapper.writeValueAsString(user))
                        .accept("application/json")
        ).andReturn().getResponse().getHeader("Authorization");
    }

    @Test
    void should_throw_a_OK_status_when_a_valid_device_is_registered() throws Exception {
        MockHttpServletResponse responseEntity = mockMvc.perform(get("/api/rule/getAll")
                        .contentType(MediaType.APPLICATION_JSON)
                        .header("Authorization", authorization)
                        .accept("application/json"))
                .andExpect(status().isOk()).andReturn().getResponse();
        List rulesList = mapper.readValue(responseEntity.getContentAsString(), List.class);

        assertTrue(rulesList.isEmpty());
    }

    @Test
    void should_throw_bad_request_error_when_you_get_all_rules_with_registered_rules() throws Exception {
        ruleService.register(new Rule("Prueba"), new LimitRuleForm());
        MockHttpServletResponse responseEntity = mockMvc.perform(get("/api/rule/getAll")
                        .contentType(MediaType.APPLICATION_JSON)
                        .header("Authorization", authorization)
                        .accept("application/json"))
                .andExpect(status().isOk()).andReturn().getResponse();
        List rulesList = mapper.readValue(responseEntity.getContentAsString(), List.class);

        assertTrue(rulesList.size() == 1);
    }


    @Test
    void should_throw_Forbidden_error_when_registered_a_device_without_authentication() throws Exception {
        mockMvc.perform(get("/api/rule/getAll")
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept("application/json"))
                .andExpect(status().isForbidden());
    }
}
