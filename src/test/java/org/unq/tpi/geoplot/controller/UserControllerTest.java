package org.unq.tpi.geoplot.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.web.servlet.MockMvc;
import org.unq.tpi.geoplot.model.User;
import org.unq.tpi.geoplot.model.dto.AuthCredentials;
import org.unq.tpi.geoplot.model.dto.MessageDTO;
import org.unq.tpi.geoplot.service.UserService;

import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
public class UserControllerTest {
    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private UserService userService;

    private final ObjectMapper mapper = new ObjectMapper();

    @BeforeEach
    public void setUp() {
        userService.clean();
    }

    @Test
    void should_trigger_a_status_OK_when_a_valid_user_registers() throws Exception {
        MockHttpServletResponse responseEntity = mockMvc.perform(post("/api/user/register")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(mapper.writeValueAsString(new AuthCredentials("prueba@gmail.com", "123456")))
                        .accept("application/json"))
                .andExpect(status().isOk()).andReturn().getResponse();
        User user = mapper.readValue(responseEntity.getContentAsString(), User.class);

        assertEquals("prueba@gmail.com", user.getEmail());
    }

    @Test
    void it_should_throw_error_BadRequest_when_registering_a_user_with_null_mail() throws Exception {
        MockHttpServletResponse responseEntity = mockMvc.perform(post("/api/user/register")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(mapper.writeValueAsString(new AuthCredentials(null, null)))
                        .accept("application/json"))
                .andExpect(status().isBadRequest()).andReturn().getResponse();
        Map message = mapper.readValue(responseEntity.getContentAsString(), Map.class);

        assertEquals("The password cannot be blank", message.get("password"));
        assertEquals("The email cannot be blank", message.get("email"));
    }

    @Test
    void should_throw_error_BadRequest_when_registering_a_user_with_an_empty_mailbox() throws Exception {
        MockHttpServletResponse responseEntity = mockMvc.perform(post("/api/user/register")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(mapper.writeValueAsString(new AuthCredentials("", null)))
                        .accept("application/json"))
                .andExpect(status().isBadRequest()).andReturn().getResponse();
        Map message = mapper.readValue(responseEntity.getContentAsString(), Map.class);

        assertEquals("The password cannot be blank", message.get("password"));
        assertEquals("The email cannot be blank", message.get("email"));
    }

    @Test
    void should_throw_error_BadRequest_when_registering_a_user_with_invalid_email_address() throws Exception {
        MockHttpServletResponse responseEntity =
                mockMvc.perform(post("/api/user/register")
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(mapper.writeValueAsString(new AuthCredentials("hola.com", null)))
                                .accept("application/json"))
                        .andExpect(status().isBadRequest()).andReturn().getResponse();
        Map message = mapper.readValue(responseEntity.getContentAsString(), Map.class);

        assertEquals("The email address is not valid", message.get("email"));
    }

    @Test
    void it_should_throw_error_BadRequest_when_registering_a_user_with_null_password() throws Exception {
        MockHttpServletResponse responseEntity = mockMvc.perform(post("/api/user/register")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(mapper.writeValueAsString(new AuthCredentials("prueba@gmail.com", null)))
                        .accept("application/json"))
                .andExpect(status().isBadRequest()).andReturn().getResponse();
        Map message = mapper.readValue(responseEntity.getContentAsString(), Map.class);

        assertEquals("The password cannot be blank", message.get("password"));
    }

    @Test
    void should_throw_error_BadRequest_when_registering_a_user_with_an_empty_password() throws Exception {
        MockHttpServletResponse responseEntity = mockMvc.perform(post("/api/user/register")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(mapper.writeValueAsString(new AuthCredentials("prueba@gmail.com", "")))
                        .accept("application/json"))
                .andExpect(status().isBadRequest()).andReturn().getResponse();
        Map message = mapper.readValue(responseEntity.getContentAsString(), Map.class);

        assertEquals("The password cannot be blank", message.get("password"));
    }

    @Test
    void should_throw_a_BadRequest_error_when_registering_a_user_with_an_already_registered_email_address() throws Exception {
        userService.register(new AuthCredentials("prueba@gmail.com", "13456"));

        MockHttpServletResponse responseEntity = mockMvc.perform(post("/api/user/register")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(mapper.writeValueAsString(new AuthCredentials("prueba@gmail.com", "25565")))
                        .accept("application/json"))
                .andExpect(status().isBadRequest()).andReturn().getResponse();
        MessageDTO message = mapper.readValue(responseEntity.getContentAsString(), MessageDTO.class);

        assertEquals("The mail has already been registered", message.getMessage());
    }
}
