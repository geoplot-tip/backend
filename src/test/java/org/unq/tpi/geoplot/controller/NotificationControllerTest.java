package org.unq.tpi.geoplot.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.web.servlet.MockMvc;
import org.unq.tpi.geoplot.model.Notification;
import org.unq.tpi.geoplot.model.User;
import org.unq.tpi.geoplot.model.dto.AuthCredentials;
import org.unq.tpi.geoplot.security.TokenUtils;
import org.unq.tpi.geoplot.service.NotificationService;
import org.unq.tpi.geoplot.service.UserService;

import java.util.List;
import java.util.Map;
import java.util.Objects;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
public class NotificationControllerTest {

    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private NotificationService notificationService;
    @Autowired
    private UserService userService;
    private final ObjectMapper mapper = new ObjectMapper();
    private final AuthCredentials user = new AuthCredentials("prueba@gmail.com", "13456");

    private String authorization;

    @BeforeEach
    public void setUp() throws Exception {
        notificationService.clean();
        userService.clean();
        userService.register(user);
        authorization = mockMvc.perform(
                post("/api/user/login")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(mapper.writeValueAsString(user))
                        .accept("application/json")
        ).andReturn().getResponse().getHeader("Authorization");
    }

    @Test
    void should_return_empty_list_when_a_user_has_no_notifications
            () throws Exception {
        MockHttpServletResponse responseEntity = mockMvc.perform(get("/api/notification/getAll")
                        .contentType(MediaType.APPLICATION_JSON)
                        .header("Authorization", authorization)
                        .accept("application/json"))
                .andExpect(status().isOk()).andReturn().getResponse();
        List notificationList = mapper.readValue(responseEntity.getContentAsString(), List.class);

        assertTrue(notificationList.isEmpty());
    }

    @Test
    void should_return_a_list_with_at_least_one_notification_when_a_user_has_registered_some() throws Exception {
        String email = Objects.requireNonNull(TokenUtils.getAuthentication(authorization.substring(7))).getPrincipal().toString();
        User user1 = userService.findByEmail(email);
        notificationService.register("danger", "Prueba", "Testing complete", user1);


        MockHttpServletResponse responseEntity = mockMvc.perform(get("/api/notification/getAll")
                        .contentType(MediaType.APPLICATION_JSON)
                        .header("Authorization", authorization)
                        .accept("application/json"))
                .andExpect(status().isOk()).andReturn().getResponse();
        List notificationList = mapper.readValue(responseEntity.getContentAsString(), List.class);

        assertEquals(1, notificationList.size());
    }

    @Test
    void should_mark_a_notification_with_valid_data_as_read() throws Exception {
        String email = Objects.requireNonNull(TokenUtils.getAuthentication(authorization.substring(7))).getPrincipal().toString();
        User user1 = userService.findByEmail(email);
        Notification notification = notificationService.register("danger", "Prueba", "Testing complete", user1);


        MockHttpServletResponse responseEntity = mockMvc.perform(post("/api/notification/markAsRead")
                        .contentType(MediaType.APPLICATION_JSON)
                        .param("notification_id",String.valueOf(notification.getId()))
                        .header("Authorization", authorization)
                        .accept("application/json"))
                .andExpect(status().isOk()).andReturn().getResponse();
        Notification notificationUpdate = mapper.readValue(responseEntity.getContentAsString(), Notification.class);

        assertTrue(notificationUpdate.isRead());
    }

    @Test
    void should_throw_a_NotFound_status_when_an_unregistered_notification() throws Exception {
        MockHttpServletResponse responseEntity = mockMvc.perform(post("/api/notification/markAsRead")
                        .contentType(MediaType.APPLICATION_JSON)
                        .param("notification_id","5")
                        .header("Authorization", authorization)
                        .accept("application/json"))
                .andExpect(status().isNotFound()).andReturn().getResponse();
        Map thrown = mapper.readValue(responseEntity.getContentAsString(), Map.class);

        assertEquals("The notification is not registered",thrown.get("message"));
    }

    @Test
    void should_throw_a_NotFound_status_when_an_unregistered_user() throws Exception {
        String email = Objects.requireNonNull(TokenUtils.getAuthentication(authorization.substring(7))).getPrincipal().toString();
        User user1 = userService.findByEmail(email);
        Notification notification = notificationService.register("danger", "Prueba", "Testing complete", user1);

        MockHttpServletResponse responseEntity = mockMvc.perform(post("/api/notification/markAsRead")
                        .contentType(MediaType.APPLICATION_JSON)
                        .param("notification_id",String.valueOf(notification.getId()))
                        .header("Authorization", "Bearer eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJhZG1pbnNzc3Nzc0BnbWFpbC5jb20iLCJleHAiOjE3ODU4ODQ0Mzl9.8LAvBkZBqbGH3NE1w2ACxSN0jv_WoYihneDfITq7DJM")
                        .accept("application/json"))
                .andExpect(status().isBadRequest()).andReturn().getResponse();
        Map thrown = mapper.readValue(responseEntity.getContentAsString(), Map.class);

        assertEquals("The user is not related to this notification",thrown.get("message"));
    }
}
