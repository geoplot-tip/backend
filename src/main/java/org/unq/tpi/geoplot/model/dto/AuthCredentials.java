package org.unq.tpi.geoplot.model.dto;

import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotBlank;
import lombok.NoArgsConstructor;

@NoArgsConstructor
public class AuthCredentials {
    @NotBlank(message = "The email cannot be blank")
    @Email(message = "The email address is not valid")
    public String email;
    @NotBlank(message = "The password cannot be blank")
    public String password;

    public AuthCredentials(String email, String password) {
        this.email = email;
        this.password = password;
    }
}
