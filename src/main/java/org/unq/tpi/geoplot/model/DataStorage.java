package org.unq.tpi.geoplot.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.RequiredArgsConstructor;

import java.util.List;

@Data
@Entity(name = "data_storage")
@Builder
@AllArgsConstructor
@RequiredArgsConstructor
public class DataStorage {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @NotBlank(message = "The name cannot be blank")
    @Column(nullable = false, unique = true)
    private String name;

    @NotNull(message = "The status cannot be null")
    @Column(nullable = false)
    @JsonIgnore
    private boolean status;

    public DataStorage(String name, boolean status) {
        this.name = name;
        this.status = status;
    }
}