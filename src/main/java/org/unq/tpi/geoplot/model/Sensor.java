package org.unq.tpi.geoplot.model;
import jakarta.persistence.*;
import jakarta.validation.constraints.NotBlank;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.RequiredArgsConstructor;

@Data
@Entity
@Builder
@RequiredArgsConstructor
@AllArgsConstructor
public class Sensor {
    @Id
    @Column(nullable = false, unique = true)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(nullable = false)
    @NotBlank(message = "The sensor type cannot be blank")
    private String type;

    @Column(nullable = false)
    @NotBlank(message = "The sensor model cannot be blank")
    private String model;

    @Column(nullable = false)
    @NotBlank(message = "The url model cannot be blank")
    private String url;

    @Column(nullable = false)
    @NotBlank(message = "The url model cannot be blank")
    private String unit;

    @Column(nullable = false)
    @NotBlank(message = "The url model cannot be blank")
    private String name;

    @Column
    private double valueSensor;

    @Column
    private double maxValue = 30.0;

    @Column
    private double minValue = 5.0;

    @Column
    private boolean prevState = true;

    @Column
    private boolean currentState = true;

    public Sensor(String type, String model, String url, String unit, String name) {
        this.type = type;
        this.model = model;
        this.url = url;
        this.name = name;
        this.unit = unit;
    }
}
