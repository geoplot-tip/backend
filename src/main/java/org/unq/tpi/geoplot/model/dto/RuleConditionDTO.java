package org.unq.tpi.geoplot.model.dto;
import lombok.Getter;

@Getter
public class RuleConditionDTO {
    public int userId;
    public double minValue;
    public double maxValue;

    public RuleConditionDTO(int userId, int minValue, int maxValue) {
        this.userId = userId;
        this.minValue = minValue;
        this.maxValue = maxValue;
    }

}
