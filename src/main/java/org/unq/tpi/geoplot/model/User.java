package org.unq.tpi.geoplot.model;
import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotBlank;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import java.util.List;

@Data
@Entity(name = "registered_user")
@Builder
@AllArgsConstructor
@RequiredArgsConstructor
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Email(message = "The email address is not valid")
    @NotBlank(message = "The email cannot be blank")
    @Column(nullable = false, unique = true)
    private String email;

    @NotBlank(message = "The password cannot be blank")
    @Column(nullable = false)
    @JsonIgnore
    private String password;

    @OneToMany(mappedBy = "user", fetch = FetchType.EAGER)
    @JsonIgnore
    private List<Notification> notifications;

    public User(String email, String password) {
        this.email = email;
        this.password = password;
    }
}