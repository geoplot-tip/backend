package org.unq.tpi.geoplot.model.dto;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.RequiredArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@RequiredArgsConstructor
public class SensorRegisterRequest {
    @NotNull(message = "The device id cannot be null")
    @Min(value = 1,message = "Device id's start from 1 onwards")
    private int deviceId;
    @NotBlank(message = "The sensor type cannot be blank")
    private String type;
    @NotBlank(message = "The sensor model cannot be blank")
    private String model;
    @NotBlank(message = "The url cannot be blank")
    private String url;
    @NotBlank(message = "The unit cannot be blank")
    private String unit;
    @NotBlank(message = "The name cannot be blank")
    private String name;
}