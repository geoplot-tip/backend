package org.unq.tpi.geoplot.model.dto;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotNull;
import lombok.NoArgsConstructor;

@NoArgsConstructor
public class OperateUserDevice {
    @NotNull(message = "The user id cannot be null")
    @Min(value = 1, message = "User id's start from 1 onwards")
    public int userId;
    @NotNull(message = "The device id cannot be null")
    @Min(value = 1, message = "Device id's start from 1 onwards")
    public int deviceId;

    public OperateUserDevice(int userId, int deviceId) {
        this.userId = userId;
        this.deviceId = deviceId;
    }
}
