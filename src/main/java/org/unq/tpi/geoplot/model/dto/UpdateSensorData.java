package org.unq.tpi.geoplot.model.dto;

import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.RequiredArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@RequiredArgsConstructor
public class UpdateSensorData {
    @NotNull(message = "The sensor id cannot be null")
    @Min(value = 1,message = "Device id's start from 1 onwards")
    private int sensorId;
    @NotBlank(message = "The device name cannot be blank")
    private String value;
}
