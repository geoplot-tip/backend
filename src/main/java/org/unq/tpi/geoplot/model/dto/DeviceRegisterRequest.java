package org.unq.tpi.geoplot.model.dto;

import jakarta.validation.constraints.NotBlank;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.RequiredArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@RequiredArgsConstructor
public class DeviceRegisterRequest {
    @NotBlank(message = "The device name cannot be blank")
    private String name;
    @NotBlank(message = "The server url cannot be blank")
    private String serverUrl;
}
