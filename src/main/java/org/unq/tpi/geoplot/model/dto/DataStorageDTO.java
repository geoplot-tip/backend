package org.unq.tpi.geoplot.model.dto;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.RequiredArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@RequiredArgsConstructor
public class DataStorageDTO {
    @NotBlank(message = "The name cannot be blank")
    private String name;

    @NotNull(message = "The status cannot be null")
    private boolean status;
}
