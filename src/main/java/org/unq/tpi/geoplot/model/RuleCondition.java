package org.unq.tpi.geoplot.model;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.RequiredArgsConstructor;

@Data
@Entity
@Builder
@AllArgsConstructor
@RequiredArgsConstructor
public class RuleCondition {
    @Id
    @Column(nullable = false, unique = true)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column
    public Double minValue;

    @Column
    public Double maxValue;

    @ManyToOne
    public User user;

    @ManyToOne
    private Rule rule;

    @ManyToOne
    private Device device;

    public RuleCondition(User user, Rule rule, Device device, double minValue, double maxValue) {
        this.minValue = minValue;
        this.maxValue = maxValue;
        this.user = user;
        this.rule = rule;
        this.device = device;
    }
}
