package org.unq.tpi.geoplot.model;
import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.RequiredArgsConstructor;

@Data
@Entity
@AllArgsConstructor
@RequiredArgsConstructor
@Builder
public class Notification {
    @Id
    @Column(nullable = false, unique = true)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(nullable = false)
    @NotBlank(message = "The variant cannot be blank")
    private  String variant;

    @Column(nullable = false)
    @NotNull(message = "The isRead cannot be blank")
    private  boolean isRead = false;

    @Column(nullable = false)
    @NotBlank(message = "The title cannot be blank")
    private  String title;

    @Column(nullable = false)
    @NotBlank(message = "The content cannot be blank")
    private  String content;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "user_id", nullable = false)
    @JsonIgnore
    private User user;

    public Notification(String variant, String title, String content, User user){
        this.variant = variant;
        this.title = title;
        this.content = content;
        this.user = user;
    }
}
