package org.unq.tpi.geoplot.model.dto;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.RequiredArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@RequiredArgsConstructor
public class DeviceInvitation {
    public int userId;
    public String email;
    public boolean isAdded;
    public int deviceId;
}
