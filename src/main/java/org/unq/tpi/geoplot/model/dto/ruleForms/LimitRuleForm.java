package org.unq.tpi.geoplot.model.dto.ruleForms;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import lombok.*;

@EqualsAndHashCode(callSuper = true)
@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class LimitRuleForm extends RuleForm {
    @Column(nullable = false)
    public int minValue;

    @Column(nullable = false)
    public int maxValue;
}
