package org.unq.tpi.geoplot.model;
import jakarta.persistence.*;
import jakarta.validation.constraints.NotBlank;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Data
@Entity
@Builder
@RequiredArgsConstructor
@AllArgsConstructor
public class Device {
    @Id
    @Column(nullable = false, unique = true)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(nullable = false)
    @NotBlank
    private String name;

    @Column(nullable = false, unique = true)
    @NotBlank
    private String serverUrl;

    @Column(nullable = false)
    private double latitude;

    @Column(nullable = false)
    private double longitude;

    @Column(nullable = false)
    private Boolean isActive;

    @Column(nullable = false)
    private Boolean hasProblems;

    @Column(nullable = false)
    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(
            name = "device_user",
            joinColumns = @JoinColumn(name = "device_id"),
            inverseJoinColumns = @JoinColumn(name = "user_id")
    )
    private List<User> userList;

    @OneToMany(cascade = CascadeType.REMOVE, fetch = FetchType.EAGER)
    @JoinTable(
            name = "device_sensor",
            joinColumns = @JoinColumn(name = "device_id"),
            inverseJoinColumns = @JoinColumn(name = "sensor_id")
    )
    private List<Sensor> sensorList;

    @OneToMany(fetch = FetchType.EAGER)
    @JoinTable(
            name = "device_rule",
            joinColumns = @JoinColumn(name = "device_id"),
            inverseJoinColumns = @JoinColumn(name = "rule_id")
    )
    private List<Rule> ruleList;

    public Device(String device_name, String server_url) {
        this.name = device_name;
        this.serverUrl = server_url;
        this.isActive = true;
        this.sensorList = new ArrayList<>();
        this.userList = new ArrayList<>();
        this.ruleList = new ArrayList<>();
        this.hasProblems = false;
    }

    public void addSensor(Sensor sensor) {
        this.sensorList.add(sensor);
    }

    public void addUser(User user) {
        userList.add(user);
    }

    public void removeUser(User user) {
        userList = userList.stream()
                .filter(user1 -> user1.getId() != user.getId())
                .collect(Collectors.toList());
    }


    public boolean userIsPresent(User user) {
        return userList.stream().anyMatch(user1 -> user1.getId() == user.getId());
    }

    public boolean ruleIsPresent(Rule rule) {
        return ruleList.stream().anyMatch(rule1 -> rule1.getId() == rule.getId());
    }
}
