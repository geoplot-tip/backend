package org.unq.tpi.geoplot.model.dto;

import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.RequiredArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@RequiredArgsConstructor
public class DeviceSetLocationRequest {
    @NotNull(message = "The device id cannot be null")
    @Min(value = 1,message = "Device id's start from 1 onwards")
    int deviceId;
    @NotNull(message = "The latitude cannot be null")
    double latitude;
    @NotNull(message = "The longitude cannot be null")
    double longitude;
}
