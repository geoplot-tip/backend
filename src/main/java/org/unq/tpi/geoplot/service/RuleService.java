package org.unq.tpi.geoplot.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.unq.tpi.geoplot.model.Rule;
import org.unq.tpi.geoplot.model.dto.ruleForms.RuleForm;
import org.unq.tpi.geoplot.repository.RuleRepository;
import org.unq.tpi.geoplot.utilities.exception.NotFoundException;

import java.util.List;

@Service
@Transactional
public class RuleService {
    @Autowired
    private RuleRepository ruleRepository;

    public Rule register(Rule rule, RuleForm ruleForm) {
        Rule registerRule = ruleRepository.save(rule);
        ruleForm.setRuleId(registerRule.getId());
        registerRule.setRuleForm(ruleForm);
        return ruleRepository.save(registerRule);
    }

    public Rule findById(int ruleId) {
        return ruleRepository.findById(ruleId).orElseThrow(() -> new NotFoundException("The rule is not registered"));
    }

    public Rule findByName(String name) {
        return ruleRepository.findByName(name).orElseThrow(() -> new NotFoundException("The rule with name is not registered"));
    }

    public List<Rule> getAll() {
        return ruleRepository.findAll();
    }

    public void clean() {
        ruleRepository.deleteAll();
    }

    public RuleForm getForm(int ruleId) {
        Rule rule = ruleRepository.findById(ruleId).orElseThrow(() -> new NotFoundException("The rule is not registered"));
        return rule.getRuleForm();
    }
}