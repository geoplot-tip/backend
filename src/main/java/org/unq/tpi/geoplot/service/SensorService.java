package org.unq.tpi.geoplot.service;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.HttpStatusCode;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;
import org.unq.tpi.geoplot.model.Sensor;
import org.unq.tpi.geoplot.model.dto.SensorRegisterRequest;
import org.unq.tpi.geoplot.model.dto.UpdateSensorData;
import org.unq.tpi.geoplot.repository.SensorRepository;
import org.unq.tpi.geoplot.utilities.exception.NotFoundException;

import java.util.Objects;

@Service
@Transactional
public class SensorService {
    @Autowired
    private SensorRepository sensorRepository;

    @Autowired
    private DeviceService deviceService;

    @Autowired
    private RestTemplate restTemplate;

    public Sensor register(@Valid SensorRegisterRequest registerRequest) {
        Sensor sensor = sensorRepository.save(new Sensor(registerRequest.getType(), registerRequest.getModel(), registerRequest.getUrl(), registerRequest.getUnit(), registerRequest.getName()));
        deviceService.addSensor(registerRequest.getDeviceId(), sensor);
        return sensor;
    }

    public Sensor findById(int sensorId) {
        return sensorRepository.findById(sensorId).orElseThrow(() -> new NotFoundException("The sensor is not registered"));
    }

    public Sensor updateData(@Valid UpdateSensorData updateSensorData) {
        Sensor sensor = this.findById(updateSensorData.getSensorId());
        sensor.setValueSensor(Double.parseDouble(updateSensorData.getValue()));
        return sensorRepository.save(sensor);
    }

    public void clean() {
        sensorRepository.deleteAll();
    }

    public void updateStatus() {
        sensorRepository.findAll().forEach(sensor -> {
            String updateUrl = sensorRepository.findDeviceBySensorId(sensor.getId()).getServerUrl() + sensor.getUrl();
            try {
                ResponseEntity<String> responseEntity = restTemplate.getForEntity(updateUrl, String.class);
                HttpStatusCode statusCode = responseEntity.getStatusCode();
                if (statusCode == HttpStatus.OK) {
                    sensor.setValueSensor(Double.parseDouble(Objects.requireNonNull(responseEntity.getBody())));
                    sensorRepository.save(sensor);
                }
            } catch (RestClientException ignored) {}
        });
    }

    public void update(Sensor sensor) {
        sensorRepository.save(sensor);
    }
}
