package org.unq.tpi.geoplot.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.unq.tpi.geoplot.model.*;
import org.unq.tpi.geoplot.model.dto.ruleForms.RuleForm;
import org.unq.tpi.geoplot.repository.RuleConditionRepository;
import org.unq.tpi.geoplot.repository.RuleRepository;
import org.unq.tpi.geoplot.security.TokenUtils;
import org.unq.tpi.geoplot.utilities.exception.DeviceException;
import org.unq.tpi.geoplot.utilities.exception.NotFoundException;

import java.util.*;

@Service
@Transactional
public class RuleConditionService {
    @Autowired
    private RuleConditionRepository ruleConditionRepository;

    public RuleCondition register(Rule rule, User user, Device device, double minValue, double maxValue) {
        if (this.existsByRuleAndUser(rule, user, device)) {
            throw new DeviceException("The condition of the rule already exists for that rule and that user");
        }
        return ruleConditionRepository.save(new RuleCondition(user, rule, device, minValue, maxValue));
    }

    public void remove(Rule rule, User user, Device device) {
        if (!this.existsByRuleAndUser(rule, user, device)) {
            throw new DeviceException("The condition of the rule not exists for that rule and that user");
        }
        ruleConditionRepository.delete(this.findByRuleAndUserAndDevice(rule, user, device));
    }

    public boolean existsByRuleAndUser(Rule rule, User user, Device device) {
        return ruleConditionRepository.existsByRuleAndUserAndDevice(rule, user, device);
    }

    public RuleCondition findByRuleAndUserAndDevice(Rule rule, User user, Device device) {
        return ruleConditionRepository.findByRuleAndUserAndDevice(rule, user, device).orElseThrow(() -> new NotFoundException("The device is not registered"));
    }

    private Optional<RuleCondition> findByRuleAndUserAndDeviceOptional(Rule rule, User user, Device device) {
        return ruleConditionRepository.findByRuleAndUserAndDevice(rule, user, device);
    }

    public List<RuleCondition> findByRuleAndDevice(Rule rule, Device device) {
        return ruleConditionRepository.findByRuleAndDevice(rule, device);
    }

    public List<Rule> getActiveRules(Device device, User user) {
        return device.getRuleList().stream().filter((rule) -> {
            Optional<RuleCondition> optionalConditionRule = findByRuleAndUserAndDeviceOptional(rule, user, device);
            return optionalConditionRule.isPresent();
        }).toList();
    }
}