package org.unq.tpi.geoplot.service;

import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;
import org.unq.tpi.geoplot.model.*;
import org.unq.tpi.geoplot.model.dto.*;
import org.unq.tpi.geoplot.repository.DeviceRepository;
import org.unq.tpi.geoplot.security.TokenUtils;
import org.unq.tpi.geoplot.utilities.exception.DeviceException;
import org.unq.tpi.geoplot.utilities.exception.NotFoundException;

import java.util.List;
import java.util.Objects;

@Service
@Transactional
public class DeviceService {
    @Autowired
    private DeviceRepository deviceRepository;

    @Autowired
    private UserService userService;

    @Autowired
    private RuleService ruleService;

    @Autowired
    private RestTemplate restTemplate;

    @Autowired
    private RuleConditionService ruleConditionService;

    public Device register(String authorization, @Valid DeviceRegisterRequest registerRequest) {
        if (deviceRepository.findByServerUrl(registerRequest.getServerUrl()).isPresent()) {
            throw new DeviceException("The device has already been registered");
        }
        String email = TokenUtils.getAuthentication(authorization.substring(7)).getPrincipal().toString();
        User user = userService.findByEmail(email);
        Device device = new Device(registerRequest.getName(), registerRequest.getServerUrl());
        device.addUser(user);

        return deviceRepository.save(device);
    }

    public Device findById(int device_id) {
        return deviceRepository.findById(device_id).orElseThrow(() -> new NotFoundException("The device is not registered"));
    }

    public void addSensor(int deviceId, Sensor sensor) {
        Device device = this.findById(deviceId);
        device.addSensor(sensor);
        deviceRepository.save(device);
    }

    public List<Device> getDevices(String authorization) {
        String email = Objects.requireNonNull(TokenUtils.getAuthentication(authorization.substring(7))).getPrincipal().toString();
        userService.findByEmail(email);
        return deviceRepository.findByUserListEmail(email).get();
    }

    public Device addUser(@Valid OperateUserDevice registerRequest) {
        User user = userService.findById(registerRequest.userId);
        Device device = this.findById(registerRequest.deviceId);
        if (device.userIsPresent(user)) {
            throw new DeviceException("The user is already included in this device");
        }
        device.addUser(user);
        return deviceRepository.save(device);
    }

    public Device removeUser(@Valid OperateUserDevice registerRequest) {
        User user = userService.findById(registerRequest.userId);
        Device device = this.findById(registerRequest.deviceId);
        if (!device.userIsPresent(user)) {
            throw new DeviceException("The user is not included in this device");
        }
        device.removeUser(user);
        return deviceRepository.save(device);
    }

    public List<DeviceInvitation> getAvailableUsers(int deviceId) {
        Device device = this.findById(deviceId);
        User user = device.getUserList().get(0);
        List<User> allUsers = userService.getAllUsers().stream().filter(user1 -> user1.getId() != user.getId()).toList();
        return allUsers.stream().map(user1 -> new DeviceInvitation(user1.getId(), user1.getEmail(), device.userIsPresent(user1), deviceId)).toList();
    }

    public Device setLocation(@Valid DeviceSetLocationRequest deviceSetLocationRequest) {
        Device device = this.findById(deviceSetLocationRequest.getDeviceId());
        device.setLatitude(deviceSetLocationRequest.getLatitude());
        device.setLongitude(deviceSetLocationRequest.getLongitude());
        return deviceRepository.save(device);
    }

    public List<Sensor> getSensors(int device_id) {
        Device device = this.findById(device_id);
        return device.getSensorList();
    }

    public void clean() {
        deviceRepository.deleteAll();
    }

    public List<Rule> addRule(int deviceId, int ruleId, RuleConditionDTO ruleConditionDTO) {
        User user = userService.findById(ruleConditionDTO.userId);
        Device device = this.findById(deviceId);
        Rule rule = ruleService.findById(ruleId);

        if(!device.ruleIsPresent(rule)) {
            ruleConditionService.register(rule, user, device, ruleConditionDTO.getMinValue(), ruleConditionDTO.getMaxValue());
            device.getRuleList().add(rule);
            deviceRepository.save(device);
        }

        return device.getRuleList();
    }

    public List<Device> findAll() {
        return deviceRepository.findAll();
    }

    public List<Rule> removeRule(int deviceId, int ruleId, int userId) {
        Device device = this.findById(deviceId);
        Rule rule = ruleService.findById(ruleId);
        User user = userService.findById(userId);

        if(device.ruleIsPresent(rule)) {
            ruleConditionService.remove(rule, user, device);
            device.getRuleList().remove(rule);
            deviceRepository.save(device);
        }

        return device.getRuleList();
    }

    public void updateStatus() {
        deviceRepository.findAll().forEach(device -> {
            String statusUrl = device.getServerUrl() + "/status";
            try {
                restTemplate.getForEntity(statusUrl, String.class);
            } catch (RestClientException exception) {
                device.setIsActive(false);
                deviceRepository.save(device);
            }
        });
    }

    public void update(Device device) {
        deviceRepository.save(device);
    }

    public List<Rule> activeRules(int deviceId, String authorization) {
        String email = Objects.requireNonNull(TokenUtils.getAuthentication(authorization.substring(7))).getPrincipal().toString();
        User user = userService.findByEmail(email);
        Device device = this.findById(deviceId);
        return ruleConditionService.getActiveRules(device, user);
    }
}