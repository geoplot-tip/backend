package org.unq.tpi.geoplot.service;

import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.unq.tpi.geoplot.model.DataStorage;
import org.unq.tpi.geoplot.model.dto.DataStorageDTO;
import org.unq.tpi.geoplot.repository.DataStorageRepository;
import org.unq.tpi.geoplot.utilities.exception.NotFoundException;

@Service
@Transactional
public class DataStorageService {
    @Autowired
    private DataStorageRepository dataStorageRepository;

    public DataStorage register(@Valid DataStorageDTO dataStorageDTO) {
        return dataStorageRepository.save(new DataStorage(dataStorageDTO.getName(), dataStorageDTO.isStatus()));
    }

    public DataStorage findById(int dataId) {
        return dataStorageRepository.findById(dataId).orElseThrow(() -> new NotFoundException("The data storage is not registered"));
    }
}