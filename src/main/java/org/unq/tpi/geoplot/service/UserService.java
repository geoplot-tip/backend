package org.unq.tpi.geoplot.service;

import jakarta.transaction.Transactional;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.unq.tpi.geoplot.model.User;
import org.unq.tpi.geoplot.model.dto.AuthCredentials;
import org.unq.tpi.geoplot.repository.UserRepository;
import org.unq.tpi.geoplot.security.PasswordEncryptor;
import org.unq.tpi.geoplot.utilities.exception.UserException;

import java.util.List;

@Service
@Transactional
public class UserService {
    @Autowired
    private UserRepository userRepository;

    public User register(@Valid AuthCredentials credentials) {
        if (userRepository.findByEmail(credentials.email).isPresent()) {
            throw new UserException("The mail has already been registered");
        }
        User user = new User(credentials.email, PasswordEncryptor.encryptPassword(credentials.password));
        return userRepository.save(user);
    }

    public User findByEmail(String email) {
        return userRepository.findByEmail(email).orElseThrow(() -> new UsernameNotFoundException("User not found"));
    }

    public void clean() {
        userRepository.deleteAll();
    }

    public User findById(int userId) {
        return userRepository.findById(userId).orElseThrow(() -> new UsernameNotFoundException("User not found"));
    }

    public List<User> getAllUsers() {
        return userRepository.findAll();
    }
}
