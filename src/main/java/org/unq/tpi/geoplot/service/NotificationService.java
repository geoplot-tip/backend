package org.unq.tpi.geoplot.service;

import jakarta.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.unq.tpi.geoplot.model.Notification;
import org.unq.tpi.geoplot.model.User;
import org.unq.tpi.geoplot.repository.NotificationRepository;
import org.unq.tpi.geoplot.security.TokenUtils;
import org.unq.tpi.geoplot.utilities.exception.NotFoundException;
import org.unq.tpi.geoplot.utilities.exception.UserException;

import java.util.Collections;
import java.util.List;
import java.util.Objects;

@Service
@Transactional
public class NotificationService {
    @Autowired
    private NotificationRepository notificationRepository;

    @Autowired
    private UserService userService;

    public Notification register(String variant, String title, String content, User user) {
        return notificationRepository.save(new Notification(variant, title, content, user));
    }

    private Notification findById(int ruleId) {
        return notificationRepository.findById(ruleId).orElseThrow(() -> new NotFoundException("The notification is not registered"));
    }

    public Notification markAsRead(int notificationId, String authorization) {
        String emailToken = Objects.requireNonNull(TokenUtils.getAuthentication(authorization.substring(7))).getPrincipal().toString();
        Notification notification = this.findById(notificationId);
        if (Objects.equals(notification.getUser().getEmail(), emailToken)) {
            notification.setRead(true);
            return notificationRepository.save(notification);
        } else {
            throw new UserException("The user is not related to this notification");
        }
    }

    public List<Notification> getNotifications(String authorization) {
        String emailToken = Objects.requireNonNull(TokenUtils.getAuthentication(authorization.substring(7))).getPrincipal().toString();
        User user = userService.findByEmail(emailToken);
        List<Notification> notifications = notificationRepository.findByUser(user);
        Collections.reverse(notifications);
        return notifications;
    }

    public void clean() {
        notificationRepository.deleteAll();
    }
}
