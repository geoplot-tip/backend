package org.unq.tpi.geoplot.initializer;

import jakarta.annotation.PostConstruct;
import org.jeasy.rules.api.Rules;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;
import org.unq.tpi.geoplot.model.dto.DataStorageDTO;
import org.unq.tpi.geoplot.model.dto.ruleForms.LimitRuleForm;
import org.unq.tpi.geoplot.rules.sensor.limitValue.Humidity;
import org.unq.tpi.geoplot.rules.sensor.limitValue.Temperature;
import org.unq.tpi.geoplot.service.*;
import org.unq.tpi.geoplot.utilities.exception.NotFoundException;

@Service
@Profile("prod")
public class RulesInitializer {
    public static final Rules rules = new Rules();
    @Autowired
    private RuleService ruleService;

    @Autowired
    private NotificationService notificationService;

    @Autowired
    private SensorService sensorService;

    @Autowired
    private MailService mailService;

    @Autowired
    private DeviceService deviceService;

    @Autowired
    private DataStorageService dataStorageService;

    @Autowired
    private RuleConditionService ruleConditionService;

    @PostConstruct
    public void initialize() {
        try {
            dataStorageService.findById(1);
        } catch (NotFoundException exception) {
            rules.register(new Humidity(notificationService, sensorService, mailService, deviceService, ruleConditionService));
            rules.register(new Temperature(notificationService, sensorService, mailService, deviceService, ruleConditionService));
            rules.forEach(r -> ruleService.register(new org.unq.tpi.geoplot.model.Rule(r.getName()), new LimitRuleForm()));
            dataStorageService.register(DataStorageDTO.builder().name("Initialize").status(true).build());
        }
    }
}