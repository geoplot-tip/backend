package org.unq.tpi.geoplot.controller;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.unq.tpi.geoplot.model.Device;
import org.unq.tpi.geoplot.model.Rule;
import org.unq.tpi.geoplot.model.RuleCondition;
import org.unq.tpi.geoplot.model.Sensor;
import org.unq.tpi.geoplot.model.dto.*;
import org.unq.tpi.geoplot.service.DeviceService;

import java.util.List;

@RestController
@RequestMapping("/api/device")
public class DeviceController {

    @Autowired
    private DeviceService deviceService;

    @PostMapping("/register")
    public ResponseEntity<Device> register(@RequestHeader("Authorization") String authorization,@Valid @RequestBody DeviceRegisterRequest registerRequest) {
        return ResponseEntity.ok(deviceService.register(authorization,registerRequest));
    }

    @PostMapping("/addUser")
    public ResponseEntity<Device> addUser(@RequestBody @Valid OperateUserDevice operationRequest) {
        return ResponseEntity.ok(deviceService.addUser(operationRequest));
    }

    @PostMapping("/removeUser")
    public ResponseEntity<Device> removeUser(@RequestBody @Valid OperateUserDevice operationRequest) {
        return ResponseEntity.ok(deviceService.removeUser(operationRequest));
    }

    @GetMapping("/availableUsers")
    public ResponseEntity<List<DeviceInvitation>> getAvailableUsers(@RequestParam("device_id") int device_id) {
        return ResponseEntity.ok(deviceService.getAvailableUsers(device_id));
    }

    @GetMapping("/devices")
    public ResponseEntity<List<Device>> getDevices(@RequestHeader("Authorization") String authorization) {
        return ResponseEntity.ok(deviceService.getDevices(authorization));
    }

    @PostMapping("/setLocation")
    public ResponseEntity<Device> setDeviceLocation(@Valid @RequestBody DeviceSetLocationRequest deviceSetLocationRequest) {
        return ResponseEntity.ok(deviceService.setLocation(deviceSetLocationRequest));
    }

    @GetMapping("/getSensors")
    public ResponseEntity<List<Sensor>> getSensors(@RequestParam("device_id") int device_id) {
        return ResponseEntity.ok(deviceService.getSensors(device_id));
    }

    @PostMapping("/addRule")
    public ResponseEntity<List<Rule>> addRule(@RequestParam("device_id") int deviceId, @RequestParam("rule_id") int ruleId, @Valid @RequestBody RuleConditionDTO ruleConditionDTO) {
        return ResponseEntity.ok(deviceService.addRule(deviceId, ruleId, ruleConditionDTO));
    }

    @PostMapping("/removeRule")
    public ResponseEntity<List<Rule>> removeRule(@RequestParam("device_id") int deviceId, @RequestParam("rule_id") int ruleId, @RequestParam("user_id") int userId) {
        return ResponseEntity.ok(deviceService.removeRule(deviceId, ruleId, userId));
    }

    @GetMapping("/activeRules")
    public ResponseEntity<List<Rule>> activeRules(@RequestHeader("Authorization") String authorization, @RequestParam("device_id") int deviceId) {
        return ResponseEntity.ok(deviceService.activeRules(deviceId, authorization));
    }
}