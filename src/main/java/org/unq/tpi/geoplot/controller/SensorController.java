package org.unq.tpi.geoplot.controller;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.unq.tpi.geoplot.model.Sensor;
import org.unq.tpi.geoplot.model.dto.SensorRegisterRequest;
import org.unq.tpi.geoplot.model.dto.UpdateSensorData;
import org.unq.tpi.geoplot.service.SensorService;

@RestController
@RequestMapping("/api/sensor")
public class SensorController {

   @Autowired
    private SensorService sensorService;

    @PostMapping("/register")
    public ResponseEntity<Sensor> register(@Valid @RequestBody SensorRegisterRequest registerRequest) {
        return ResponseEntity.ok(sensorService.register(registerRequest));
    }

    @PostMapping("/updateData")
    public ResponseEntity<Sensor> updateData(@Valid @RequestBody UpdateSensorData updateSensorData) {
        return ResponseEntity.ok(sensorService.updateData(updateSensorData));
    }
}
