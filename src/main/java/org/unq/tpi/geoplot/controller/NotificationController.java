package org.unq.tpi.geoplot.controller;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.unq.tpi.geoplot.model.Notification;
import org.unq.tpi.geoplot.service.NotificationService;

import java.util.List;

@RestController
@RequestMapping("/api/notification")
public class NotificationController {

    @Autowired
    private NotificationService notificationService;

    @GetMapping("/getAll")
    public ResponseEntity<List<Notification>> getNotifications(@RequestHeader("Authorization") String authorization) {
        return ResponseEntity.ok(notificationService.getNotifications(authorization));
    }

    @PostMapping("/markAsRead")
    public ResponseEntity<Notification> markAsRead(@RequestParam("notification_id") int notificationId, @RequestHeader("Authorization") String authorization) {
        return ResponseEntity.ok(notificationService.markAsRead(notificationId, authorization));
    }


}
