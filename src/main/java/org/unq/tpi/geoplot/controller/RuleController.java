package org.unq.tpi.geoplot.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.unq.tpi.geoplot.model.Device;
import org.unq.tpi.geoplot.model.Rule;
import org.unq.tpi.geoplot.model.dto.ruleForms.RuleForm;
import org.unq.tpi.geoplot.service.RuleService;

import java.util.List;

@RestController
@RequestMapping("/api/rule")
public class RuleController {

    @Autowired
    private RuleService ruleService;

    @GetMapping("/getAll")
    public ResponseEntity<List<Rule>> getAll() {
        return ResponseEntity.ok(ruleService.getAll());
    }

    @GetMapping("/form")
    public ResponseEntity<RuleForm> getForm(@RequestParam("rule_id") int ruleId) {
        return ResponseEntity.ok(ruleService.getForm(ruleId));
    }
}