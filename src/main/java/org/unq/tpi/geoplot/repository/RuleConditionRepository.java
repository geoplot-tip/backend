package org.unq.tpi.geoplot.repository;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.unq.tpi.geoplot.model.Device;
import org.unq.tpi.geoplot.model.Rule;
import org.unq.tpi.geoplot.model.RuleCondition;
import org.unq.tpi.geoplot.model.User;

import java.util.List;
import java.util.Optional;

@Repository
public interface RuleConditionRepository extends JpaRepository<RuleCondition, Integer> {
    List<RuleCondition> findByRule(Rule rule);
    boolean existsByRuleAndUserAndDevice(Rule rule, User user, Device device);
    Optional<RuleCondition> findByRuleAndUserAndDevice(Rule rule, User user, Device device);
    List<RuleCondition> findByRuleAndDevice(Rule rule, Device device);
}