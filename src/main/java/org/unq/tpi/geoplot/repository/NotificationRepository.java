package org.unq.tpi.geoplot.repository;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.unq.tpi.geoplot.model.Notification;
import org.unq.tpi.geoplot.model.User;
import java.util.List;

@Repository
public interface NotificationRepository extends JpaRepository<Notification, Integer> {
    List<Notification> findByUser(User user);
}
