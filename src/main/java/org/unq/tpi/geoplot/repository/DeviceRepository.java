package org.unq.tpi.geoplot.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.unq.tpi.geoplot.model.Device;

import java.util.List;
import java.util.Optional;

@Repository
public interface DeviceRepository extends JpaRepository<Device, Integer> {
    Optional<Device> findByServerUrl(String server_url);

    Optional<List<Device>> findByUserListEmail(String mail);

  /*  @Query("SELECT device FROM Device device WHERE device.owner.email = :searched_user_id")
    List<Device> findByUserId(@Param("searched_user_id") String user_id);*/
}