package org.unq.tpi.geoplot.repository;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.unq.tpi.geoplot.model.Device;
import org.unq.tpi.geoplot.model.Sensor;

@Repository
public interface SensorRepository extends JpaRepository<Sensor, Integer> {
    @Query("SELECT d FROM Device d JOIN d.sensorList s WHERE s.id = :sensorId")
    Device findDeviceBySensorId(@Param("sensorId") int sensorId);
}