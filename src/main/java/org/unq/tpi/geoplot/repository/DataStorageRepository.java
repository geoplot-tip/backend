package org.unq.tpi.geoplot.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.unq.tpi.geoplot.model.DataStorage;

@Repository
public interface DataStorageRepository extends JpaRepository<DataStorage, Integer> {
}