package org.unq.tpi.geoplot.rules.sensor.limitValue;
import org.jeasy.rules.annotation.Rule;
import org.unq.tpi.geoplot.service.*;

import java.lang.annotation.Annotation;

@Rule(name = "Limit Value - Humidity Rule")
public class Humidity extends LimitValue {

    public Humidity(NotificationService notificationService, SensorService sensorService, MailService mailService, DeviceService deviceService, RuleConditionService ruleConditionService) {
        super("Humidity".toUpperCase(), notificationService, sensorService, mailService, deviceService, ruleConditionService, "Limit Value - Humidity Rule");
    }
}
