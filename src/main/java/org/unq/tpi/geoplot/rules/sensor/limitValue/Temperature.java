package org.unq.tpi.geoplot.rules.sensor.limitValue;
import org.jeasy.rules.annotation.Rule;
import org.unq.tpi.geoplot.service.*;

@Rule(name = "Limit Value - Temperature Rule")
public class Temperature extends LimitValue {

    public Temperature(NotificationService notificationService, SensorService sensorService, MailService mailService, DeviceService deviceService, RuleConditionService ruleConditionService) {
        super("Temperature".toUpperCase(), notificationService, sensorService, mailService, deviceService, ruleConditionService, "Limit Value - Temperature Rule");
    }
}