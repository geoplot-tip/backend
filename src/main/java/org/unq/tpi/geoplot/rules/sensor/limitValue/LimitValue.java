package org.unq.tpi.geoplot.rules.sensor.limitValue;
import jakarta.mail.MessagingException;
import lombok.Data;
import org.jeasy.rules.annotation.Action;
import org.jeasy.rules.annotation.Condition;
import org.jeasy.rules.annotation.Fact;
import org.jeasy.rules.annotation.Rule;
import org.jeasy.rules.api.Rules;
import org.unq.tpi.geoplot.model.Device;
import org.unq.tpi.geoplot.model.RuleCondition;
import org.unq.tpi.geoplot.model.Sensor;
import org.unq.tpi.geoplot.model.User;
import org.unq.tpi.geoplot.service.*;
import org.unq.tpi.geoplot.utilities.MessageHTML;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

import static org.unq.tpi.geoplot.utilities.MessageHTML.generateHTMLList;

@Rule
public class LimitValue extends Rules {

    private final NotificationService notificationService;
    private final DeviceService deviceService;
    private final SensorService sensorService;
    private final MailService mailService;
    private final RuleConditionService ruleConditionService;
    private final String sensorType;
    private final String ruleName;
    private List<Sensor> inadequateSensors;
    private List<Sensor> adequateSensors;
    private List<RuleCondition> adequateRuleConditions;
    private List<RuleCondition> inadequateRuleConditions;


    @Condition
    public boolean condition(@Fact("device") Device device) {
        Optional<org.unq.tpi.geoplot.model.Rule> optionalRule = device.getRuleList().stream().filter((rule) ->
                Objects.equals(rule.getName(), ruleName)).findFirst();

        if(optionalRule.isPresent()) {
            List<Sensor> sensorList = device.getSensorList();

            adequateSensors = sensorList.stream().filter((sensor) -> {
                adequateRuleConditions = ruleConditionService.findByRuleAndDevice(optionalRule.get(), device)
                        .stream().filter((ruleCondition -> sensor.getValueSensor() > ruleCondition.minValue &&
                                sensor.getValueSensor() < ruleCondition.maxValue)).toList();


                return (Objects.equals(sensor.getType().toUpperCase(), sensorType) &&
                        (sensor.isPrevState() && !sensor.isCurrentState()) &&
                         !adequateRuleConditions.isEmpty());
            }).collect(Collectors.toList());


            inadequateSensors = sensorList.stream().filter((sensor) -> {
                inadequateRuleConditions = ruleConditionService.findByRuleAndDevice(optionalRule.get(), device)
                        .stream().filter((ruleCondition ->
                                sensor.getValueSensor() <= ruleCondition.minValue ||
                                sensor.getValueSensor() >= ruleCondition.maxValue)).toList();

                return (Objects.equals(sensor.getType().toUpperCase(), sensorType) &&
                        ((sensor.isPrevState() && sensor.isCurrentState()  ||
                          !sensor.isPrevState() && sensor.isCurrentState())) &&
                           !inadequateRuleConditions.isEmpty());
            }).collect(Collectors.toList());
        }

        return !inadequateSensors.isEmpty() || !adequateSensors.isEmpty();
    }

    @Action
    public void action(@Fact("device") Device device) throws MessagingException {
        Optional<org.unq.tpi.geoplot.model.Rule> optionalRule = device.getRuleList().stream().filter(rule ->
                Objects.equals(rule.getName(), this.ruleName)).findFirst();

        if(optionalRule.isPresent() && !inadequateSensors.isEmpty()) {
            this.actionToMarkInadequateSensors(inadequateSensors, device, inadequateRuleConditions);
        }
        if(optionalRule.isPresent() && !adequateSensors.isEmpty()) {
            this.actionToMarkAdequateSensors(adequateSensors, device,  adequateRuleConditions);
        }
    }

    private void actionToMarkInadequateSensors(List<Sensor> sensors, Device device, List<RuleCondition> ruleConditions) {
        sensors.forEach(sensor -> {
            sensor.setPrevState(sensor.isCurrentState());
            sensor.setCurrentState(false);
            sensorService.update(sensor);

            ruleConditions.forEach(ruleCondition -> {
                String title = String.format("The %s sensor of type %s senses an out-of-range value.", sensor.getName(), this.sensorType);
                String content = String.format("Alert generated in the device %s, the sensor %s sensed values outside the allowed range [%s%s - %s%s - %s%s]", device.getName(), sensor.getName(), sensor.getMinValue(),sensor.getUnit(), sensor.getValueSensor(), sensor.getUnit(), sensor.getMaxValue(), sensor.getUnit());
                notificationService.register("danger", title, content, ruleCondition.user);
                try {
                    mailService.sendMail(
                            String.format("Inappropriate values in device %s", device.getName()),
                            ruleCondition.user.getEmail(),
                            MessageHTML.TEMPLATE_MAIL(
                                    "The following sensors showed values outside normal parameters",
                                    "Dear user, " + content,
                                    generateHTMLList(this.inadequateSensors.stream().map(Sensor::getName).toList())
                            )
                    );
                } catch (MessagingException e) {
                    throw new RuntimeException(e);
                }
            });
        });
        device.setHasProblems(true);
        deviceService.update(device);
    }

    private void actionToMarkAdequateSensors(List<Sensor> sensors, Device device, List<RuleCondition> ruleConditions) {
        sensors.forEach(sensor -> {
            sensor.setPrevState(sensor.isCurrentState());
            sensor.setCurrentState(true);
            sensorService.update(sensor);
            ruleConditions.forEach(ruleCondition -> {
                String title = String.format("The %s sensor of type %s has returned to normal values.", sensor.getName(), this.sensorType);
                String content = String.format("Alert generated in the device %s, the sensor %s sensed values inside the allowed range [%s%s - %s%s - %s%s]", device.getName(), sensor.getName(), sensor.getMinValue(),sensor.getUnit(), sensor.getValueSensor(), sensor.getUnit(), sensor.getMaxValue(), sensor.getUnit());
                notificationService.register("success", title, content, ruleCondition.user);
                try {
                    mailService.sendMail(
                            String.format("Appropriate values in device %s", device.getName()),
                            ruleCondition.user.getEmail(),
                            MessageHTML.TEMPLATE_MAIL(
                                    "The following sensors returned values within normal parameters",
                                    "Dear user, " + content,
                                    generateHTMLList(this.adequateSensors.stream().map(Sensor::getName).toList())
                            )
                    );
                } catch (MessagingException e) {
                    throw new RuntimeException(e);
                }
            });
        });
        device.setHasProblems(false);
        deviceService.update(device);
    }

    public LimitValue(String sensorType, NotificationService notificationService, SensorService sensorService,
                      MailService mailService, DeviceService deviceService, RuleConditionService ruleConditionService,
                      String ruleName) {
        this.ruleConditionService = ruleConditionService;
        this.notificationService = notificationService;
        this.sensorService = sensorService;
        this.mailService = mailService;
        this.sensorType = sensorType;
        this.deviceService = deviceService;
        this.ruleName = ruleName;
    }
}