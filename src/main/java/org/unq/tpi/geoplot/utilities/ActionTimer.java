package org.unq.tpi.geoplot.utilities;

import org.jeasy.rules.api.Facts;
import org.jeasy.rules.api.Rule;
import org.jeasy.rules.api.Rules;
import org.jeasy.rules.api.RulesEngine;
import org.jeasy.rules.core.DefaultRulesEngine;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.unq.tpi.geoplot.initializer.RulesInitializer;
import org.unq.tpi.geoplot.model.Device;
import org.unq.tpi.geoplot.service.DeviceService;
import org.unq.tpi.geoplot.service.SensorService;

import java.util.List;
import java.util.Objects;

@Component
@EnableScheduling
public class ActionTimer {
    private final long SEGUNDO = 1000;
    private final long MINUTO = SEGUNDO * 60;

    @Autowired
    private DeviceService deviceService;

    @Autowired
    private SensorService sensorService;

    @Scheduled(fixedDelay = 60 * SEGUNDO)
    public void executeRules() {
        RulesEngine rulesEngine = new DefaultRulesEngine();
        List<Device> devices = deviceService.findAll();
        Rules rules = RulesInitializer.rules;
        devices.forEach(device -> {
            Rules rulesList = new Rules();
            for (Rule rule : rules) {
                if (device.getRuleList().stream().anyMatch(r -> Objects.equals(r.getName(), rule.getName()))) {
                    rulesList.register(rule);
                }
            }
            Facts facts = new Facts();
            facts.put("device", device);
            rulesEngine.fire(rulesList, facts);
        });
    }

    @Scheduled(fixedDelay = 40 * SEGUNDO)
    public void updateEquipment() {
        deviceService.updateStatus();
        sensorService.updateStatus();
    }
}