package org.unq.tpi.geoplot.utilities.exception;

public class DeviceException extends RuntimeException {
    public DeviceException(String message) {
        super(message);
    }
}
