package org.unq.tpi.geoplot.utilities.exception;

public class SensorException extends RuntimeException {
    public SensorException(String message) {
        super(message);
    }
}
