package org.unq.tpi.geoplot.utilities.exception;

public class UserException extends RuntimeException {
    public UserException(String message) {
        super(message);
    }
}
