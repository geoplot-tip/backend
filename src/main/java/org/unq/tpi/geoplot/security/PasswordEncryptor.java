package org.unq.tpi.geoplot.security;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

public class PasswordEncryptor {
    private static BCryptPasswordEncoder encryptor = new BCryptPasswordEncoder();

    public static String encryptPassword(String password){
        return encryptor.encode(password);
    }
}
