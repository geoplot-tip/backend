#include <SoftwareSerial.h>
#include <DHT.h>
#include <TinyGPS++.h>

#define DHTPIN 12
#define DHTTYPE DHT11

SoftwareSerial Serial_Communication_To_ESP(6, 5);
SoftwareSerial gps_serial_connection(2, 3);

DHT dht(DHTPIN, DHTTYPE);
TinyGPSPlus gps;

void setup() {
  gps_serial_connection.begin(9600);
  Serial_Communication_To_ESP.begin(9600);
  Serial.begin(9600);
  dht.begin();
}

void loop() {
  if (Serial_Communication_To_ESP.available() > 0) {
    String intruction = Serial_Communication_To_ESP.readStringUntil('\n');

    int indexOfTheBlankSpace = intruction.indexOf(' ');
    String actionReference = intruction.substring(0, indexOfTheBlankSpace);
    String sensorReference = intruction.substring(indexOfTheBlankSpace + 1);
  
    if (actionReference.equals("GET")) {

        if(sensorReference.equals("TEMPERATURE")) {
          Serial_Communication_To_ESP.print(dht.readTemperature());
        }

        if(sensorReference.equals("HUMIDITY")) {
          Serial_Communication_To_ESP.print(dht.readHumidity());
        }

        if(sensorReference.equals("GPS")) {
          while (gps_serial_connection.available() > 0) {
            char c = gps_serial_connection.read();
            Serial.print(c);
          }
        }
    }
  }
}
