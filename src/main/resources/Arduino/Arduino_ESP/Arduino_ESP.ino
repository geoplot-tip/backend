#include <ESP8266WiFi.h>
#include <ESP8266WebServer.h>
#include <ESP8266HTTPClient.h>
#include <SoftwareSerial.h>
#include <EEPROM.h>
#include <ArduinoJson.h>

const int eepromSize = 512;
const int ssidMaxSize = 32;
const int passwordMaxSize = 64;
const int userEmailMaxSize = 96;
const int userPasswordMaxSize = 128;
const int deviceIdMaxSize = 160;
const int dataLoadedMaxSize = 161;
const char* serverAddress = "http://192.168.1.37:8085";

String userToken;
int temperatureSensorId;
int humiditySensorId;

String temperatureSensorUrl = "/sensor/temperature/getData";
String humiditySensorUrl = "/sensor/humidity/getData";


char ssid[ssidMaxSize];
char password[passwordMaxSize];
char userEmail[userEmailMaxSize];
char userPassword[userPasswordMaxSize];
char deviceId[deviceIdMaxSize];

WiFiClient client;
HTTPClient http;

ESP8266WebServer server(80);
SoftwareSerial Serial_Communication_To_Arduino_UNO(D5,D6);

int connectionAttempts = 0;

void setup() {
  Serial.begin(115200);
  Serial_Communication_To_Arduino_UNO.begin(9600);
  server.begin();
  EEPROM.begin(eepromSize);
  bool loadedData = EEPROM.read(dataLoadedMaxSize) == 1;

  if(loadedData) {
    EEPROM.get(0, ssid);
    EEPROM.get(ssidMaxSize, password);
    WiFi.begin(ssid, password);
    while (WiFi.status() != WL_CONNECTED) {
      delay(1000);
      Serial.println("Status: Establishing network connection. Please Wait");
      connectionAttempts++;
      if(connectionAttempts == 20) {
        break;
      }
    }
    if(WiFi.status() == WL_CONNECTED) {
      EEPROM.get(passwordMaxSize, userEmail);
      EEPROM.get(userEmailMaxSize, userPassword);
      Serial.println("Status: Connection established");
      Serial.println(WiFi.localIP());
      loginUser();
      delay(5000);
      registerDevice();
    }
  }

  if(WiFi.status() != WL_CONNECTED) {
    WiFi.mode(WIFI_AP);
    WiFi.softAP("GEOPLOT-DEVICE");
  }

  server.on("/", HTTP_GET, configurationSite);
  server.on("/configure", HTTP_POST, networkDataUpload);
  server.on("/reset", HTTP_POST, resetCredentials);
  server.on(temperatureSensorUrl, HTTP_GET, sendDataFromTemperatureSensor);
  server.on(humiditySensorUrl, HTTP_GET, sendDataFromHumiditySensor);
  server.on("/status", HTTP_GET, sendStatus);
}

void loop() {
  server.handleClient();
}


/*******************************/
/*        AUTH SECTION         */
/*******************************/

void loginUser() {
  String loginPath = "/api/user/login";
  String urlToLogin = serverAddress + loginPath; 

  http.begin(client, urlToLogin);
  http.addHeader("Content-Type", "application/json");
  
  const char *headerKeys[] = {"Authorization"};
  const size_t headerKeysCount = sizeof(headerKeys) / sizeof(headerKeys[0]);
  http.collectHeaders(headerKeys, headerKeysCount);

  StaticJsonDocument<200> jsonDoc;
  jsonDoc["email"] = String(userEmail);
  jsonDoc["password"] = String(userPassword);

  String requestBody;
  serializeJson(jsonDoc, requestBody);

  int httpResponseCode = http.POST(requestBody);

  if(httpResponseCode == HTTP_CODE_OK) {
    userToken = http.header("Authorization");
    http.end();
  } else {
    Serial.println("ERROR: Token Not obtained");
    http.end();
  }
}

void registerDevice() {
  String registerPath = "/api/device/register";
  String urlToRegister = serverAddress + registerPath;
  
  http.begin(client, urlToRegister);
  http.addHeader("Authorization", userToken);
  http.addHeader("Content-Type", "application/json");

  StaticJsonDocument<200> jsonDoc;
  jsonDoc["name"] = "Arduino-UNQ-POC";
  jsonDoc["serverUrl"] = "http://" + WiFi.localIP().toString() + ":80";

  String requestBody;
  serializeJson(jsonDoc, requestBody);

  int httpResponseCode = http.POST(requestBody);

  if (httpResponseCode == HTTP_CODE_OK) {
    DynamicJsonDocument jsonDoc(512);
    DeserializationError error = deserializeJson(jsonDoc, http.getString());
    int deviceIdResult = jsonDoc["id"];
    temperatureSensorId = registerSensor(deviceIdResult, temperatureSensorUrl, "TEMPERATURE", "DHT11", "°C", "Temperature S1");
    humiditySensorId = registerSensor(deviceIdResult, humiditySensorUrl, "HUMIDITY", "DHT11", "Hr", "Humidity S1");
    setLocation(deviceIdResult);
    http.end();
  } else {
    Serial.println("ERROR: DEVICE NOT REGISTERED");
    http.end();
  }
}

String setLocation(int device_id) {
  String setLocationPath = "/api/device/setLocation";
  String urlToSetLocation = serverAddress + setLocationPath;

  http.begin(client, urlToSetLocation);
  http.addHeader("Authorization", userToken);
  http.addHeader("Content-Type", "application/json");

  StaticJsonDocument<200> jsonDoc;
  jsonDoc["deviceId"] = device_id;
  jsonDoc["latitude"] = -34.712311037972555;
  jsonDoc["longitude"] = -58.289240598678596;

  String requestBody;
  serializeJson(jsonDoc, requestBody);

  int httpResponseCode = http.POST(requestBody);

  if (httpResponseCode == HTTP_CODE_OK) {
    return http.getString();
  } else {
    return "Fail";
  }

  http.end();
}

int registerSensor(int device_id, String url, String sensor_type, String model, String unit ,String name) {
  String registerPath = "/api/sensor/register";
  String urlToRegister = serverAddress + registerPath;

  http.begin(client, urlToRegister);
  http.addHeader("Authorization", userToken);
  http.addHeader("Content-Type", "application/json");

  StaticJsonDocument<200> jsonDoc;
  jsonDoc["deviceId"] = device_id;
  jsonDoc["url"] = url;
  jsonDoc["type"] = sensor_type;
  jsonDoc["model"] = model;
  jsonDoc["unit"] = unit;
  jsonDoc["name"] = name;

  String requestBody;
  serializeJson(jsonDoc, requestBody);

  int httpResponseCode = http.POST(requestBody);
  
  if (httpResponseCode == HTTP_CODE_OK) {
    return http.getString().toInt();
  } else {
    return 0;
  }

  http.end();
}

/*******************************/
/*  SEND DATA SENSORS SECTION  */
/*******************************/

String sendDataFromTemperatureSensor() {
  Serial_Communication_To_Arduino_UNO.print("GET TEMPERATURE");

  
  while (!Serial_Communication_To_Arduino_UNO.available()) {
    delay(10);
  }

  String intruction = Serial_Communication_To_Arduino_UNO.readStringUntil('\n');

  server.send(200, "application/json", intruction);

  return intruction;
}

String sendDataFromHumiditySensor() {
  Serial_Communication_To_Arduino_UNO.print("GET HUMIDITY");
  
  while (!Serial_Communication_To_Arduino_UNO.available()) {
    delay(10);
  }

  String intruction = Serial_Communication_To_Arduino_UNO.readStringUntil('\n');

  server.send(200, "application/json", intruction);

  return intruction;
}

bool sendStatus() {
  server.send(200, "application/json", "true");
  return true;
}

/******************************/
/* WIFI CONFIGURATION SECTION */
/******************************/

void configurationSite() {
  String html = "<html><head><style>";
  html += "body {";
  html += "  background-color: #50bc6c;";
  html += "  text-align: center;";
  html += "}";
  html += "form {";
  html += "  display: flex;";
  html += "  flex-direction: column;";
  html += "  align-items: center;";
  html += "}";
  html += "</style></head><body>";
  html += "<h1>WiFi configuration</h1>";
  html += "<form method='post' action='/configure'>";
  html += "Network name: <input type='text' name='ssid' value='" + String(ssid) + "'><br>";
  html += "Password: <input type='password' name='password' value='" + String(password) + "'><br>";
  html += "Geoplot email: <input type='text' name='userEmail' value='" + String(userEmail) + "'><br>";
  html += "Geoplot password: <input type='password' name='userPassword' value='" + String(userPassword) + "'><br>";
  html += "<input type='submit' value='Save'>";
  html += "</form>";
  html += "</body></html>";
  server.send(200, "text/html", html);
}


void networkDataUpload() {
  String newSSID = server.arg("ssid");
  String newPassword = server.arg("password");
  String newUserEmail = server.arg("userEmail");
  String newUserPassword = server.arg("userPassword");

  strncpy(ssid, newSSID.c_str(), ssidMaxSize);
  strncpy(password, newPassword.c_str(), passwordMaxSize);
  strncpy(userEmail, newUserEmail.c_str(), userEmailMaxSize);
  strncpy(userPassword, newUserPassword.c_str(), userPasswordMaxSize);

  ssid[ssidMaxSize - 1] = '\0';
  password[passwordMaxSize - 1] = '\0';
  userEmail[userEmailMaxSize - 1] = '\0';
  userPassword[userPasswordMaxSize - 1] = '\0';

  EEPROM.put(0, ssid);
  EEPROM.put(ssidMaxSize, password);
  EEPROM.put(passwordMaxSize, userEmail);
  EEPROM.put(userEmailMaxSize, userPassword);
  EEPROM.write(dataLoadedMaxSize, 1);
  EEPROM.commit();

  String message = "Saved configuration. The device will restart shortly. Remember that if it failed to establish a connection to your wifi network, you can restart the device to repeat the process.";
  server.send(200, "text/plain", message);

  delay(10000);
  ESP.restart();
}

void resetCredentials() {
  ssid[ssidMaxSize - 1] = '\0';
  password[passwordMaxSize - 1] = '\0';
  userEmail[userEmailMaxSize - 1] = '\0';
  userPassword[userPasswordMaxSize - 1] = '\0';

  EEPROM.put(0, ssid);
  EEPROM.put(ssidMaxSize, password);
  EEPROM.put(passwordMaxSize, userEmail);
  EEPROM.put(userEmailMaxSize, userPassword);
  EEPROM.write(dataLoadedMaxSize, 0);
  EEPROM.commit();

  String message = "The data has been reset. The device will restart shortly. Remember that to load the data you must access the device's wifi access point and then use the device's ip address as the address.";
  server.send(200, "text/plain", message);

  delay(10000);
  ESP.restart();
}

bool isNumber(const String& str) {
  for (char c : str) {
    if (!isdigit(c)) {
      return false;
    }
  }
  return true;
}
