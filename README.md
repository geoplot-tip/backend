# Geo-Plot

## Description
The idea of Geo-Plot is to be able to link a number of devices to the wev client in order to be able to manage crop plots on a map.

### Requirements:
- You must have OpenJDK 20 installed: https://openjdk.org/install/
- Have the **lombok** plugin installed in the IDE.
- PostgreSQL database is used as a database. However, any database can be used through the environment variables.
- Docker compose has been incorporated. In this way, it is not required to create a database beforehand.

## Usage
Access the Main.java class, which is located in src/main/java/org/unq/tpi/ 
Once inside the class, run the application by clicking on the green button. You can also run the application by right clicking on the Main.java file and then press "Run Main.java".

## Contributing
Fernando Dodino

## Authors and acknowledgment
Contardo Juan Pablo
Germán Greco Ventura