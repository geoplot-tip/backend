# Usa la imagen base de Maven 3.9.1 y OpenJDK 20 para compilar la aplicación
FROM jelastic/maven:3.9.1-openjdk-20 AS builder
# Copia el archivo POM y el archivo de código fuente
COPY pom.xml /build/
COPY src /build/src/

# Establece el directorio de trabajo en /build
WORKDIR /build

# Empaqueta la aplicación como un archivo JAR
RUN mvn package -Pprod

# Ahora usa la imagen oficial de Amazon Corretto 20.0.0 para ejecutar la aplicación
FROM amazoncorretto:20.0.0
EXPOSE 8085
ENV PORT 8085
# Copia el script waitingDatabase.sh al directorio /app
COPY waitingDatabase.sh /app/waitingDatabase.sh

# Instala ncat (parte de nmap) para esperar a que un servicio esté disponible
RUN yum install -y nmap-ncat

# Copia el archivo JAR generado desde la imagen anterior
COPY --from=builder /build/target/geoplot-1.0-SNAPSHOT.jar /app/app.jar

# Establece el directorio de trabajo en /app
WORKDIR /app

# Otorga permisos de ejecución al script
RUN chmod +x waitingDatabase.sh

# Comando para ejecutar la aplicación Spring Boot
CMD ["sh", "-c", "/app/waitingDatabase.sh"]