echo "Initiating the wait to complete the database"

while ! nc -z ${POSTGRES_HOST} 5432; do sleep 1; done

echo "Finished waiting to complete database"

java -jar app.jar --server.port=${PORT}
